/**
 * Created on: Jul 13, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.savedrequest.DefaultSavedRequest;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.web.context.request.NativeWebRequest;

/**
 * @author Hieu Nguyen
 *
 */
public abstract class ResolveClientParamController {

  final RequestCache requestCache;

  /**
   * @param requestCache
   */
  public ResolveClientParamController(RequestCache requestCache) {
    this.requestCache = requestCache;
  }

  final protected Map<String, String> resolveClientParams(final NativeWebRequest nwRequest) {
    Map<String, String> maps = new HashMap<String, String>();

    HttpServletRequest httpReq = nwRequest.getNativeRequest(HttpServletRequest.class);
    HttpServletResponse httpRes = nwRequest.getNativeResponse(HttpServletResponse.class);
    final DefaultSavedRequest defaultSavedRequest = (DefaultSavedRequest) requestCache.getRequest(httpReq,
        httpRes);

    if (defaultSavedRequest != null) {
      final String[] clientIds = defaultSavedRequest.getParameterValues("client_id");
      final String[] redirectUris = defaultSavedRequest.getParameterValues("redirect_uri");

      if (clientIds != null && clientIds.length > 0)
        maps.put("client_id", clientIds[0]);
      else
        maps.put("client_id", null);

      if (redirectUris != null && redirectUris.length > 0)
        maps.put("redirect_uri", redirectUris[0]);
      else
        maps.put("redirect_uri", null);
    }

    return maps;
  }

  final protected RequestCache getRequestCache() {
    return this.requestCache;
  }

}
