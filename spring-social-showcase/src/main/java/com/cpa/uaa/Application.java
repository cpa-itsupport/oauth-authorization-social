package com.cpa.uaa;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;

@ComponentScan(basePackages = { "com.cpa.uaa", "org.springframework.social.google" })
@EnableAutoConfiguration
@EnableConfigurationProperties
public class Application {

  public static void main(String[] args) {
    new SpringApplicationBuilder().bannerMode(Banner.Mode.OFF).sources(Application.class).run(args);
  }

  @Bean
  public RequestCache requestCache() {
    return new HttpSessionRequestCache();
  }

}