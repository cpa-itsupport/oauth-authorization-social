/**
 * Created on: Jul 14, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.security.recaptcha;

import org.springframework.core.NestedRuntimeException;

/**
 * @author Hieu Nguyen
 *
 */
public class GoogleRecaptchaException extends NestedRuntimeException {

  private static final long serialVersionUID = -4982903381048857918L;

  /**
   * @param msg
   */
  public GoogleRecaptchaException(String msg) {
    super(msg);
  }
}
