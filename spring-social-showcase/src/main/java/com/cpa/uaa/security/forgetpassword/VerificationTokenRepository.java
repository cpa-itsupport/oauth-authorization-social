/**
 * Created on: Jun 29, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.security.forgetpassword;

import java.util.Optional;

/**
 * @author Hieu Nguyen
 *
 */
public interface VerificationTokenRepository {

  void createVerificationToken(VerificationToken verificationToken);

  Optional<VerificationToken> findVerificationTokenByToken(String token);

  int updateTokenExpiryNow(final String token);
}
