/**
 * Created on: Jul 4, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.security.recaptcha;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Hieu Nguyen
 *
 */
@Component
@ConfigurationProperties(prefix = "google.recaptcha.key")
public class GoogleRecaptchaProperties {
  
  public static final String CAPTCHA_RESPONSE_VAR = "g-recaptcha-response";

  private String client;

  private String server;
  
  private String verifyurl;
  
  /**
   * @return the verifyurl
   */
  public String getVerifyurl() {
    return verifyurl;
  }

  /**
   * @param verifyurl the verifyurl to set
   */
  public void setVerifyurl(String verifyurl) {
    this.verifyurl = verifyurl;
  }

  /**
   * @return the client
   */
  public String getClient() {
    return client;
  }

  /**
   * @param client the client to set
   */
  public void setClient(String client) {
    this.client = client;
  }

  /**
   * @return the server
   */
  public String getServer() {
    return server;
  }

  /**
   * @param server the server to set
   */
  public void setServer(String server) {
    this.server = server;
  }
}
