/**
 * Created on: May 6, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.security;

/**
 * @author Hieu Nguyen
 *
 */
public interface PasswordEncryptor {

  public String encrypt(String algorithm, String plainTextPassword, String encryptedPassword);

  public String[] getSupportedAlgorithmTypes();

}
