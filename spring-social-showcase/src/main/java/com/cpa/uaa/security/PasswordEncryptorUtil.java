/**
 * Created on: May 6, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Hieu Nguyen
 *
 */
public class PasswordEncryptorUtil {

  public static final String DEFAULT_PASSWORDS_ENCRYPTION_ALGORITHM = "PBKDF2WITHHMACSHA1/160/128000";

  public static final String TYPE_BCRYPT = "BCRYPT";

  /**
   * @deprecated As of 6.1.0, replaced by {@link #TYPE_UFC_CRYPT}
   */
  public static final String TYPE_CRYPT = "CRYPT";

  public static final String TYPE_MD2 = "MD2";

  public static final String TYPE_MD5 = "MD5";

  public static final String TYPE_NONE = "NONE";

  public static final String TYPE_PBKDF2 = "PBKDF2";

  public static final String TYPE_SHA = "SHA";

  public static final String TYPE_SHA_256 = "SHA-256";

  public static final String TYPE_SHA_384 = "SHA-384";

  public static final String TYPE_SSHA = "SSHA";

  public static final String TYPE_UFC_CRYPT = "UFC-CRYPT";

  public static String encrypt(String plainTextPassword) {

    return encrypt(plainTextPassword, null);
  }

  public static String encrypt(String plainTextPassword, String encryptedPassword) {

    long startTime = 0;

    if (_log.isDebugEnabled()) {
      startTime = System.currentTimeMillis();
    }

    try {
      return encrypt(DEFAULT_PASSWORDS_ENCRYPTION_ALGORITHM, plainTextPassword, encryptedPassword);
    }
    finally {
      if (_log.isDebugEnabled()) {
        _log.debug("Password encrypted in " + (System.currentTimeMillis() - startTime) + "ms");
      }
    }
  }

  public static String encrypt(String algorithm, String plainTextPassword, String encryptedPassword) {

    return _passwordEncryptor.encrypt(algorithm, plainTextPassword, encryptedPassword);
  }

  public PasswordEncryptor getPasswordEncryptor() {
    return _passwordEncryptor;
  }

  public void setPasswordEncryptor(PasswordEncryptor passwordEncryptor) {
    _passwordEncryptor = passwordEncryptor;
  }

  private static Logger _log = LoggerFactory.getLogger(PasswordEncryptorUtil.class);

  private static PasswordEncryptor _passwordEncryptor;

  public static byte[] longToBytes(long l) {
    byte[] result = new byte[8];
    for (int i = 7; i >= 0; i--) {
      result[i] = (byte) (l & 0xFF);
      l >>= 8;
    }
    return result;
  }

  public static long bytesToLong(byte[] b) {
    long result = 0;
    for (int i = 0; i < 8; i++) {
      result <<= 8;
      result |= (b[i] & 0xFF);
    }
    return result;
  }
}
