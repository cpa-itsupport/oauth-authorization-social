/**
 * Created on: Jul 13, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.security.forgetpassword;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.cpa.uaa.signin.LocalAccountSignInService;

/**
 * @author Hieu Nguyen
 *
 */
@Service
@Transactional
public class VerficationTokenServiceImpl implements VerificationTokenService {

  final VerificationTokenRepository verificationTokenRepository;

  final LocalAccountSignInService localAccountSignInService;

  /**
   * @param verificationTokenRepository
   * @param localAccountSignInService
   */
  public VerficationTokenServiceImpl(VerificationTokenRepository verificationTokenRepository,
      LocalAccountSignInService localAccountSignInService) {
    this.verificationTokenRepository = verificationTokenRepository;
    this.localAccountSignInService = localAccountSignInService;
  }

  /* (non-Javadoc)
   * @see com.cpa.uaa.security.forgetpassword.VerificationTokenService#validateToken(java.lang.String)
   */
  @Override
  public VerificationToken validateToken(final String token) {
    final VerificationToken verificationToken = verificationTokenRepository
        .findVerificationTokenByToken(token).orElseThrow(() -> new InvalidTokenException("Token not found."));

    if (verificationToken.getExpiryDateTime().isBefore(LocalDateTime.now())) {
      throw new InvalidTokenException("Token is expired.");
    }

    // once the validation is success, expires the token.
    verificationTokenRepository.updateTokenExpiryNow(token);
    localAccountSignInService.signInWithTokenCredential(verificationToken.getUserId(), token);

    return verificationToken;
  }

  @Override
  public Optional<String> getRedirectUri(final String token) {

    final Optional<VerificationToken> verificationToken = verificationTokenRepository
        .findVerificationTokenByToken(token);
    String redirectUri = null;

    if (verificationToken.isPresent() && StringUtils.hasText(verificationToken.get().getRedirectUri())
        && StringUtils.hasText(verificationToken.get().getClientId())) {

      redirectUri = "redirect:/oauth/authorize?response_type=token&client_id="
          + verificationToken.get().getClientId() + "&redirect_uri="
          + verificationToken.get().getRedirectUri();
    }

    return Optional.ofNullable(redirectUri);
  }

  @Override
  public void createVerificationToken(VerificationToken verificationToken) {
    verificationTokenRepository.createVerificationToken(verificationToken);
  }
}
