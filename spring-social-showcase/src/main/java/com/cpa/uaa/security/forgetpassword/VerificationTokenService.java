/**
 * Created on: Jul 13, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.security.forgetpassword;

import java.util.Optional;

/**
 * @author Hieu Nguyen
 *
 */
public interface VerificationTokenService {

  VerificationToken validateToken(String token);

  Optional<String> getRedirectUri(final String token);

  void createVerificationToken(VerificationToken verificationToken);

}