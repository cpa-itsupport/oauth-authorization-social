/**
 * Created on: Jul 4, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.security.forgetpassword;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cpa.tins.client.ArrayOfKeyValuePair;
import com.cpa.tins.client.Exception_Exception;
import com.cpa.tins.client.MailParamsBuilder;
import com.cpa.tins.client.MessageWebServiceImplPortType;
import com.cpa.uaa.account.Account;
import com.cpa.uaa.account.AccountService;

/**
 * @author Hieu Nguyen
 *
 */
@Service
public class ForgetPasswordServiceImpl implements ForgetPasswordService {

  private static final Logger logger = LoggerFactory.getLogger(ForgetPasswordServiceImpl.class);

  final VerificationTokenRepository verificationTokenRepository;

  final AccountService accountService;

  final MessageSource messages;

  final PasswordEncoder passwordEncoder;

  final MessageWebServiceImplPortType tinsServiceClient;

  @Value("${mail.reset.password.url}")
  private String url;

  /**
   * 
   * @param verificationTokenRepository
   * @param accountService
   * @param mailSender
   * @param messages
   * @param passwordEncoder
   * @param tinsServiceClient
   */
  public ForgetPasswordServiceImpl(VerificationTokenRepository verificationTokenRepository,
      AccountService accountService, MessageSource messages, PasswordEncoder passwordEncoder,
      MessageWebServiceImplPortType tinsServiceClient) {
    this.verificationTokenRepository = verificationTokenRepository;
    this.accountService = accountService;
    this.messages = messages;
    this.passwordEncoder = passwordEncoder;
    this.tinsServiceClient = tinsServiceClient;
  }

  @Override
  public void sendResetPasswordMail(final String email, final String token) {
    final Account account = accountService.findByUsernameOrId(email);
    //@formatter:off
    final ArrayOfKeyValuePair mailParams = new MailParamsBuilder()
        .from(messages.getMessage("mail.support.address", null, Locale.getDefault()))
        .to(email)
        .withStringParam("user", account.getFullName())
        .withStringParam("forget_password_uri", url.trim() + token)
        .withSubject(messages.getMessage("mail.reset.password.subject", null, Locale.getDefault())).build();
    //@formatter:on
    try {
      final Long sendEmail = tinsServiceClient.sendEmail("EPS", "CGY_FORGET_PASSWORD", "CGY_FORGET_PASSWORD",
          mailParams);
    }
    catch (Exception_Exception e) {
      logger.error("Unable to send email using TINS", e);
    }
  }

  @Override
  @Transactional
  public void updateNewPassword(final String userId, final String newPassword) {
    accountService.updatePassword(userId, newPassword);
  }

  @Override
  public boolean isCurrentPasswordValid(final String plainPassword) {
    return passwordEncoder.matches(plainPassword, accountService.getCurrentAccount().getPassword());
  }
}
