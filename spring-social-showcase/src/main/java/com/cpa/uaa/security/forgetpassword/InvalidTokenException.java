/**
 * Created on: Jul 4, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.security.forgetpassword;

import org.springframework.core.NestedRuntimeException;

/**
 * @author Hieu Nguyen
 *
 */
public class InvalidTokenException extends NestedRuntimeException {

  /**
   * @param msg
   */
  public InvalidTokenException(String msg) {
    super(msg);
  }

  private static final long serialVersionUID = -7211652662398067566L;

}
