/**
 * Created on: Jun 27, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.security;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.StringUtils;

/**
 * @author Hieu Nguyen
 *
 */
public class LiferayPasswordPbkdf2Encoder implements PasswordEncoder {

  private final PasswordEncryptorUtil passwordEncryptorUtil;

  public LiferayPasswordPbkdf2Encoder() {
    passwordEncryptorUtil = new PasswordEncryptorUtil();
    passwordEncryptorUtil.setPasswordEncryptor(new PBKDF2PasswordEncryptor());
  }

  /* (non-Javadoc)
   * @see org.springframework.security.crypto.password.PasswordEncoder#encode(java.lang.CharSequence)
   */
  @Override
  public String encode(CharSequence rawPassword) {
    return PasswordEncryptorUtil.encrypt(rawPassword.toString(), null);
  }

  /* (non-Javadoc)
   * @see org.springframework.security.crypto.password.PasswordEncoder#matches(java.lang.CharSequence, java.lang.String)
   */
  @Override
  public boolean matches(CharSequence rawPassword, String encryptedPassword) {
    if (!StringUtils.hasText(rawPassword))
      return false;
    return PasswordEncryptorUtil.encrypt(rawPassword.toString(), encryptedPassword).equals(encryptedPassword);
  }

}
