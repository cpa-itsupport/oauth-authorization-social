/**
 * Created on: Jul 4, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.security.forgetpassword;

import javax.validation.constraints.Size;

/**
 * @author Hieu Nguyen
 *
 */
public class UpdatePasswordForm {

  private String oldPassword;

  @Size(min = 8, message = "must be at least 8 characters")
  private String newPassword;

  private String confirmPassword;
  
  private boolean redirectUriEnabled = false;

  /**
   * @return the redirectUriEnabled
   */
  public boolean isRedirectUriEnabled() {
    return redirectUriEnabled;
  }

  /**
   * @param redirectUriEnabled the redirectUriEnabled to set
   */
  public void setRedirectUriEnabled(boolean redirectUriEnabled) {
    this.redirectUriEnabled = redirectUriEnabled;
  }

  /**
   * @return the oldPassword
   */
  public String getOldPassword() {
    return oldPassword;
  }

  /**
   * @param oldPassword the oldPassword to set
   */
  public void setOldPassword(String oldPassword) {
    this.oldPassword = oldPassword;
  }

  /**
   * @return the newPassword
   */
  public String getNewPassword() {
    return newPassword;
  }

  /**
   * @param newPassword the newPassword to set
   */
  public void setNewPassword(String newPassword) {
    this.newPassword = newPassword;
  }

  /**
   * @return the confirmPassword
   */
  public String getConfirmPassword() {
    return confirmPassword;
  }

  /**
   * @param confirmPassword the confirmPassword to set
   */
  public void setConfirmPassword(String confirmPassword) {
    this.confirmPassword = confirmPassword;
  }
}
