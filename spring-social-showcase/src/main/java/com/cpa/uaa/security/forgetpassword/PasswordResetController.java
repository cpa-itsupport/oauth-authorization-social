/**
 * Created on: Jun 29, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.security.forgetpassword;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.cpa.uaa.account.Account;
import com.cpa.uaa.account.AccountService;
import com.cpa.uaa.controller.ResolveClientParamController;
import com.cpa.uaa.message.Message;
import com.cpa.uaa.message.MessageType;
import com.cpa.uaa.security.recaptcha.GoogleRecaptchaException;
import com.cpa.uaa.security.recaptcha.GoogleRecaptchaProperties;
import com.cpa.uaa.security.recaptcha.GoogleRecaptchaService;
import com.cpa.uaa.web.WebUtilsServiceImpl;

/**
 * @author Hieu Nguyen
 *
 */
@Controller()
public class PasswordResetController extends ResolveClientParamController {

  private static final Logger logger = LoggerFactory.getLogger(PasswordResetController.class);

  final VerificationTokenService verificationTokenService;

  final ForgetPasswordService forgetPasswordService;

  final GoogleRecaptchaService googleRecaptchaService;

  final AccountService accountService;

  /**
   * @param requestCache
   * @param verificationTokenService
   * @param forgetPasswordService
   * @param googleRecaptchaService
   * @param accountService
   */
  @Inject
  public PasswordResetController(RequestCache requestCache, VerificationTokenService verificationTokenService,
      ForgetPasswordService forgetPasswordService, GoogleRecaptchaService googleRecaptchaService,
      com.cpa.uaa.account.AccountService accountService) {
    super(requestCache);
    this.verificationTokenService = verificationTokenService;
    this.forgetPasswordService = forgetPasswordService;
    this.googleRecaptchaService = googleRecaptchaService;
    this.accountService = accountService;
  }

  @GetMapping("/forgetPassword")
  public void forgetPassword() {

  }

  @GetMapping("/changePassword")
  public UpdatePasswordForm changePassword(final HttpServletRequest hsRequest,
      final HttpServletResponse hsResponse) {
    final String redirectUri = hsRequest.getParameter(WebUtilsServiceImpl.REDIRECT_URI);
    final UpdatePasswordForm updatePasswordForm = new UpdatePasswordForm();

    if (StringUtils.hasText(redirectUri)) {
      final SavedRequest savedRequest = getRequestCache().getRequest(hsRequest, hsResponse);
      if (savedRequest == null)
        getRequestCache().saveRequest(hsRequest, hsResponse);

      updatePasswordForm.setRedirectUriEnabled(true);
    }

    return updatePasswordForm;
  }

  @GetMapping({ "/updatePassword" })
  public UpdatePasswordForm updatePassword() {
    return new UpdatePasswordForm();
  }

  @RequestMapping(value = "/forgetPassword", method = RequestMethod.POST)
  public void resetPassword(final NativeWebRequest nwRequest, @RequestParam("email")
  final String userEmail) {
    final Account account;
    try {
      account = accountService.findByUsernameOrId(userEmail);

      if (!account.isEnabled())
        throw new DisabledException("Account is disabled.");

      final String recaptchaRes = nwRequest.getParameter(GoogleRecaptchaProperties.CAPTCHA_RESPONSE_VAR);
      googleRecaptchaService.validate(recaptchaRes);

      final Map<String, String> clientParams = resolveClientParams(nwRequest);
      final VerificationToken passwordToken = new VerificationToken(account.getId(),
          clientParams.get("client_id"), clientParams.get("redirect_uri"));

      verificationTokenService.createVerificationToken(passwordToken);
      forgetPasswordService.sendResetPasswordMail(userEmail, passwordToken.getToken());

      nwRequest.setAttribute("message",
          new Message(MessageType.INFO, "An email has been sent to you, please check your email in a few minutes."),
          WebRequest.SCOPE_REQUEST);
    }
    catch (EmptyResultDataAccessException e) {
      nwRequest.setAttribute("message", new Message(MessageType.ERROR, "The username is invalid."),
          WebRequest.SCOPE_REQUEST);
    }
    catch (DisabledException e) {
      nwRequest.setAttribute("message",
          new Message(MessageType.ERROR,
              "You cannot reset the password since your account has been disabled."),
          WebRequest.SCOPE_REQUEST);
    }
    catch (GoogleRecaptchaException e) {
      nwRequest.setAttribute("message",
          new Message(MessageType.ERROR, "Could you verify recaptcha information."),
          WebRequest.SCOPE_REQUEST);
    }
  }

  @GetMapping("/resetPassword")
  public String resetPassword(@RequestParam
  final String token, final NativeWebRequest nwRequest) {
    String returnUrl;
    try {
      verificationTokenService.validateToken(token);
      returnUrl = "forward:/updatePassword";
    }
    catch (Exception e) {
      logger.info("Could not validate reset password token.", e);
      nwRequest.setAttribute("message",
          new Message(MessageType.ERROR, "Invalid token to update your password."), WebRequest.SCOPE_REQUEST);
      returnUrl = "forward:/signin";
    }
    return returnUrl;
  }

  @PostMapping(value = { "/updatePassword" })
  public String updatePassword(@Valid UpdatePasswordForm updatePasswordForm, final BindingResult formBinding,
      final WebRequest wRequest, Authentication authentication) {

    if (formBinding.hasErrors()) {
      return null;
    }

    final Account account = accountService.getCurrentAccount();
    final String token = (String) authentication.getCredentials();
    String redirectUri = null;

    if (updatePasswordForm.getNewPassword().equals(updatePasswordForm.getConfirmPassword())) {
      redirectUri = verificationTokenService.getRedirectUri(token).orElse(null);
      forgetPasswordService.updateNewPassword(account.getId(), updatePasswordForm.getNewPassword());
      wRequest.setAttribute("message",
          new Message(MessageType.INFO, "Your password has been changed successfully."),
          WebRequest.SCOPE_REQUEST);
    }
    else {
      formBinding.rejectValue("confirmPassword", "password.notmatch", "Confirm password does not match.");
      return null;
    }

    return redirectUri;
  }

  @PostMapping(value = "/changePassword", params = "action=save")
  public String changePassword(@Valid UpdatePasswordForm updatePasswordForm, final BindingResult formBinding,
      final NativeWebRequest wRequest, Authentication authentication) {
    final String redirectUri;

    final HttpServletRequest hsRequest = wRequest.getNativeRequest(HttpServletRequest.class);
    final HttpServletResponse hsResponse = wRequest.getNativeResponse(HttpServletResponse.class);
    final SavedRequest savedRequest = getRequestCache().getRequest(hsRequest, hsResponse);
    if (savedRequest != null && StringUtils.hasText(savedRequest.getRedirectUrl())) {
      updatePasswordForm.setRedirectUriEnabled(true);
    }

    if (formBinding.hasErrors()) {
      redirectUri = null;
    }
    else if (!StringUtils.hasText(updatePasswordForm.getOldPassword())
        || !forgetPasswordService.isCurrentPasswordValid(updatePasswordForm.getOldPassword())) {
      formBinding.rejectValue("oldPassword", "password.invalid", "Old password is incorrect.");
      redirectUri = null;
    }
    else if (!updatePasswordForm.getNewPassword().equals(updatePasswordForm.getConfirmPassword())) {
      formBinding.rejectValue("confirmPassword", "password.notmatch", "Confirm password does not match.");
      redirectUri = null;
    }
    else {
      final Account account = accountService.getCurrentAccount();
      forgetPasswordService.updateNewPassword(account.getId(), updatePasswordForm.getNewPassword());
      wRequest.setAttribute("message",
          new Message(MessageType.INFO, "Your password has been changed successfully."),
          WebRequest.SCOPE_REQUEST);
      redirectUri = null;
    }

    return redirectUri;
  }

  @PostMapping(value = "/changePassword", params = "action=goback")
  public String goBack(final HttpServletRequest hsRequest, final HttpServletResponse hsResponse) {
    final Optional<String> redirectUri = Optional
        .ofNullable(getRequestCache().getRequest(hsRequest, hsResponse)).map(sr -> {
          final UriComponents build = UriComponentsBuilder.fromHttpUrl(sr.getRedirectUrl()).build();
          String uri = build.getQueryParams().get(WebUtilsServiceImpl.REDIRECT_URI).get(0);
          uri = uri + "?token="
              + build.getQueryParams().getOrDefault(WebUtilsServiceImpl.TOKEN, Arrays.asList("")).get(0);
          return uri;
        });

    return "redirect:" + redirectUri.orElse("/connect/changePassword");
  }

}
