/**
 * Created on: Jul 4, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.security.forgetpassword;

/**
 * @author Hieu Nguyen
 *
 */
public interface ForgetPasswordService {

  void sendResetPasswordMail(final String email, final String token);

  void updateNewPassword(final String userId, final String newPassword);

  boolean isCurrentPasswordValid(final String plainPassword);

}