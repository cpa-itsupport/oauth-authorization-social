/**
 * Created on: Jul 4, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.security.recaptcha;

/**
 * @author Hieu Nguyen
 *
 */
public interface GoogleRecaptchaService {

  void validate(String response);

}