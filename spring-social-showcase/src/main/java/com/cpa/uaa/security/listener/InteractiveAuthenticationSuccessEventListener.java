/**
 * Created on: Aug 2, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.security.listener;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.cpa.uaa.signin.LocalAccountSignInService;
import com.cpa.uaa.signin.SignInDetails;
import com.cpa.uaa.web.WebUtilsService;

/**
 * @author Hieu Nguyen
 *
 */
@Service
public class InteractiveAuthenticationSuccessEventListener {

  private final LocalAccountSignInService localAccountSignInService;

  private final WebUtilsService webUtilsService;

  /**
   * @param localAccountSignInService
   * @param webUtilsService
   */
  public InteractiveAuthenticationSuccessEventListener(LocalAccountSignInService localAccountSignInService,
      WebUtilsService webUtilsService) {
    this.localAccountSignInService = localAccountSignInService;
    this.webUtilsService = webUtilsService;
  }

  @EventListener
  public void handleInteractiveAuthenticationSuccessEvent(final InteractiveAuthenticationSuccessEvent event) {
    final HttpServletRequest curReq = ((ServletRequestAttributes) RequestContextHolder
        .currentRequestAttributes()).getRequest();

    localAccountSignInService.updateSigninDetails(new SignInDetails(webUtilsService.getClientIp(curReq),
        ((User) event.getAuthentication().getPrincipal()).getUsername()));
  }
}
