/**
 * Created on: May 5, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.security;

import java.nio.ByteBuffer;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.springframework.util.Base64Utils;
import org.springframework.util.StringUtils;

/**
 * @author Hieu Nguyen
 *
 */
public class PBKDF2PasswordEncryptor implements PasswordEncryptor {

  public String[] getSupportedAlgorithmTypes() {
    return new String[] { PasswordEncryptorUtil.TYPE_PBKDF2 };
  }

  protected String doEncrypt(String algorithm, String plainTextPassword, String encryptedPassword) {

    try {
      PBKDF2EncryptionConfiguration pbkdf2EncryptionConfiguration = new PBKDF2EncryptionConfiguration();

      pbkdf2EncryptionConfiguration.configure(algorithm, encryptedPassword);

      byte[] saltBytes = pbkdf2EncryptionConfiguration.getSaltBytes();

      final int rounds = pbkdf2EncryptionConfiguration.getRounds();
      final int keySize = pbkdf2EncryptionConfiguration.getKeySize();
      PBEKeySpec pbeKeySpec = new PBEKeySpec(plainTextPassword.toCharArray(), saltBytes, rounds, keySize);

      String algorithmName = algorithm;

      int index = algorithm.indexOf('/');

      if (index > -1) {
        algorithmName = algorithm.substring(0, index);
      }

      SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(algorithmName);
      SecretKey secretKey = secretKeyFactory.generateSecret(pbeKeySpec);
      byte[] secretKeyBytes = secretKey.getEncoded();

      ByteBuffer byteBuffer = ByteBuffer.allocate(2 * 4 + saltBytes.length + secretKeyBytes.length);
      byteBuffer.putInt(keySize);
      byteBuffer.putInt(rounds);
      byteBuffer.put(saltBytes);
      byteBuffer.put(secretKeyBytes);

      return Base64Utils.encodeToString(byteBuffer.array());
    }
    catch (Exception e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  private static final int _KEY_SIZE = 160;

  private static final int _ROUNDS = 128000;

  private static final int _SALT_BYTES_LENGTH = 8;

  private static Pattern _pattern = Pattern.compile("^.*/?([0-9]+)?/([0-9]+)$");

  private class PBKDF2EncryptionConfiguration {

    public void configure(String algorithm, String encryptedPassword) {
      if (StringUtils.isEmpty(encryptedPassword)) {
        Matcher matcher = _pattern.matcher(algorithm);

        if (matcher.matches()) {
          _keySize = (StringUtils.hasText(matcher.group(1))) ? Integer.parseInt(matcher.group(1)) : _KEY_SIZE;
          _rounds = (StringUtils.hasText(matcher.group(2))) ? Integer.parseInt(matcher.group(2)) : _ROUNDS;
        }

        long randomLong = ThreadLocalRandom.current().nextLong();
        _saltBytes = PasswordEncryptorUtil.longToBytes(randomLong);
      }
      else {
        byte[] bytes = new byte[16];

        try {
          byte[] encryptedPasswordBytes = Base64Utils.decodeFromString(encryptedPassword);

          System.arraycopy(encryptedPasswordBytes, 0, bytes, 0, bytes.length);
        }
        catch (Exception e) {
          throw new RuntimeException();
        }

        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);

        _keySize = byteBuffer.getInt();
        _rounds = byteBuffer.getInt();

        byteBuffer.get(_saltBytes);
      }
    }

    public int getKeySize() {
      return _keySize;
    }

    public int getRounds() {
      return _rounds;
    }

    public byte[] getSaltBytes() {
      return _saltBytes;
    }

    private int _keySize = _KEY_SIZE;

    private int _rounds = _ROUNDS;

    private byte[] _saltBytes = new byte[_SALT_BYTES_LENGTH];

  }

  /* (non-Javadoc)
   * @see com.cpa.liferay.security.PasswordEncryptor#encrypt(java.lang.String, java.lang.String, java.lang.String)
   */
  @Override
  public String encrypt(String algorithm, String plainTextPassword, String encryptedPassword) {
    if (StringUtils.isEmpty(plainTextPassword)) {
      throw new RuntimeException("Unable to encrypt blank password");
    }

    return doEncrypt(algorithm, plainTextPassword, encryptedPassword);
  }
}
