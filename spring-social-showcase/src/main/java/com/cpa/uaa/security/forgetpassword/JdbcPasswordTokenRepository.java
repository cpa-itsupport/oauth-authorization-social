/**
 * Created on: Jun 29, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.security.forgetpassword;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Optional;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Hieu Nguyen
 *
 */
@Repository
public class JdbcPasswordTokenRepository implements VerificationTokenRepository {

  private final JdbcTemplate jdbcTemplate;

  private static final Logger logger = LoggerFactory.getLogger(JdbcPasswordTokenRepository.class);

  @Inject
  public JdbcPasswordTokenRepository(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  /* (non-Javadoc)
   * @see com.cpa.uaa.security.forgetpassword.PasswordTokenRepository#createPasswordToken(com.cpa.uaa.security.forgetpassword.PasswordToken)
   */
  @Override
  public void createVerificationToken(VerificationToken passwordToken) {
    jdbcTemplate.update(
        "insert into verification_token (token, user_id, expiry_at, client_id, redirect_uri) values (?, ?, ?, ?, ?)",
        passwordToken.getToken(), passwordToken.getUserId(), passwordToken.getExpiryTimestamp(),
        passwordToken.getClientId(), passwordToken.getRedirectUri());

  }

  /* (non-Javadoc)
   * @see com.cpa.uaa.security.forgetpassword.PasswordTokenRepository#findPasswordTokenByToken(java.lang.String)
   */
  @Override
  public Optional<VerificationToken> findVerificationTokenByToken(String token) {
    VerificationToken pt = null;
    //@formatter:off
    try {
      pt = jdbcTemplate.queryForObject(
          "select token, user_id, expiry_at, client_id, redirect_uri from verification_token where token = ?"
          ,(rs, rn) -> new VerificationToken(rs.getString("token"), rs.getString("user_id"),
                                         rs.getTimestamp("expiry_at"), rs.getString("client_id"), rs.getString("redirect_uri"))
          ,token);
    //@formatter:on
    }
    catch (Exception e) {
      logger.info("Unable to find token.");
    }

    return Optional.ofNullable(pt);
  }

  @Override
  @Transactional
  public int updateTokenExpiryNow(final String token) {
    return jdbcTemplate.update("update verification_token set expiry_at = ? where token = ?",
        Date.from(ZonedDateTime.now().toInstant()), token);
  }

}
