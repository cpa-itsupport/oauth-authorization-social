/**
 * Created on: Jul 4, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.security.recaptcha;

import java.net.URI;
import java.util.regex.Pattern;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

/**
 * @author Hieu Nguyen
 *
 */
@Service
public class GoogleRecaptchaServiceImpl implements GoogleRecaptchaService {

  private static final Pattern RESPONSE_PATTERN = Pattern.compile("[A-Za-z0-9_-]+");

  GoogleRecaptchaProperties googleRecaptchaProperties;

  RestTemplate restOperations;

  @Inject
  public GoogleRecaptchaServiceImpl(GoogleRecaptchaProperties googleRecaptchaProperties) {
    this.googleRecaptchaProperties = googleRecaptchaProperties;
    this.restOperations = new RestTemplate();
  }

  /* (non-Javadoc)
   * @see com.cpa.uaa.security.recaptcha.GoogleRecaptchaService#processResponse(java.lang.String)
   */
  @Override
  public void validate(String response) {
    if (!responseSanityCheck(response)) {
      throw new GoogleRecaptchaException("Response contains invalid characters");
    }

    final String urlString = String.format(
        googleRecaptchaProperties.getVerifyurl() + "?secret=%s&response=%s",
        googleRecaptchaProperties.getServer(), response);

    URI verifyUri = URI.create(urlString);

    GoogleRecaptchaResponse googleResponse = restOperations.getForObject(verifyUri,
        GoogleRecaptchaResponse.class);

    if (!googleResponse.isSuccess()) {
      throw new GoogleRecaptchaException("reCaptcha was not successfully validated");
    }
  }

  private boolean responseSanityCheck(String response) {
    return StringUtils.hasLength(response) && RESPONSE_PATTERN.matcher(response).matches();
  }

}
