/**
 * Created on: Jun 29, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.security.forgetpassword;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author Hieu Nguyen
 *
 */
public class VerificationToken {

  private static final long RESET_PASSWORD_TOKEN_VALID_MINUTES = TimeUnit.HOURS.toMinutes(8);

  private final String token;

  private final String userId;

  private final LocalDateTime expiryDateTime;

  private final String redirectUri;

  private final String clientId;

  /**
   * 
   */
  public VerificationToken() {
    this.token = UUID.randomUUID().toString();
    this.expiryDateTime = LocalDateTime.now().plusMinutes(RESET_PASSWORD_TOKEN_VALID_MINUTES);
    this.redirectUri = null;
    this.userId = null;
    this.clientId = null;
  }

  /**
   * 
   */
  public VerificationToken(final String userId, final String clientId, final String redirectUri) {
    this.token = UUID.randomUUID().toString();
    this.userId = userId;
    this.expiryDateTime = LocalDateTime.now().plusMinutes(RESET_PASSWORD_TOKEN_VALID_MINUTES);
    this.redirectUri = redirectUri;
    this.clientId = clientId;
  }

  /**
   * @param token
   * @param userId
   * @param expiryTimestamp
   */
  public VerificationToken(final String token, final String userId, final Timestamp expiryTimestamp,
      final String clientId, final String redirectUri) {
    this.token = token;
    this.userId = userId;
    this.expiryDateTime = expiryTimestamp.toLocalDateTime();
    this.redirectUri = redirectUri;
    this.clientId = clientId;
  }

  /**
   * @return the token
   */
  public String getToken() {
    return token;
  }

  /**
   * @return the userId
   */
  public String getUserId() {
    return userId;
  }

  /**
   * @return the expiryDateTime
   */
  public LocalDateTime getExpiryDateTime() {
    return expiryDateTime;
  }

  public Timestamp getExpiryTimestamp() {
    return Timestamp.valueOf(expiryDateTime);
  }

  /**
   * @return the redirectUri
   */
  public String getRedirectUri() {
    return redirectUri;
  }

  /**
   * @return the clientId
   */
  public String getClientId() {
    return clientId;
  }

}
