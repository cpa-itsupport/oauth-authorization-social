/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cpa.uaa.config;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;

import com.cpa.uaa.security.LiferayPasswordPbkdf2Encoder;
import com.cpa.uaa.signin.AuthenticationExceptionErrorMessageAuthenticationFailureHandler;

/**
 * Security Configuration.
 * @author Craig Walls
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  @Inject
  private DataSource dataSource;

  @Autowired
  public void registerAuthentication(AuthenticationManagerBuilder auth) throws Exception {
    auth.jdbcAuthentication().dataSource(dataSource)
        .usersByUsernameQuery("select id as username, password, enabled from Account where username = ?")
        .authoritiesByUsernameQuery("select id as username, 'ROLE_USER' from Account where id = ?")
        .passwordEncoder(passwordEncoder());
  }

  @Override
  public void configure(WebSecurity web) throws Exception {
    web.ignoring().antMatchers("/**/*.css", "/**/*.png", "/**/*.gif", "/**/*.jpg", "/**/*.js", "/**/*.ico");
  }

  //@formatter:off
	@Override
	protected void configure(HttpSecurity http) throws Exception {
    http      
			.formLogin()
				.loginPage("/signin")
				.loginProcessingUrl("/signin/authenticate")
				.defaultSuccessUrl("/connect")				
				.failureHandler(new AuthenticationExceptionErrorMessageAuthenticationFailureHandler(exceptionErrorCodeMap()))
			.and()			  
				.logout()				  
					.logoutUrl("/signout")
					.invalidateHttpSession(true)
					.deleteCookies("JSESSIONID")
					.logoutSuccessUrl("/signin")
			.and()			  
				.authorizeRequests()
					.antMatchers("/", "/webjars/**", "/admin/**", "/favicon.ico", "/resources/**", "/auth/**", 
					             "/signin/**", "/signup/**", "/disconnect/facebook", "/health", 
					             "/forgetPassword","/resetPassword", "/exit").permitAll()
					.antMatchers("/**").authenticated()	
			;
	}
	 //@formatter:on

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new LiferayPasswordPbkdf2Encoder();
  }

  @Bean
  public TextEncryptor textEncryptor() {
    return Encryptors.noOpText();
  }

  @Bean
  public SpringSecurityDialect springSecurityDialect() {
    return new SpringSecurityDialect();
  }

  public Map<Class<? extends AuthenticationException>, String> exceptionErrorCodeMap() {
    Map<Class<? extends AuthenticationException>, String> exceptionErrorCodeMap = new HashMap<Class<? extends AuthenticationException>, String>();
    exceptionErrorCodeMap.put(DisabledException.class, "disabled_account");

    return exceptionErrorCodeMap;
  }
}
