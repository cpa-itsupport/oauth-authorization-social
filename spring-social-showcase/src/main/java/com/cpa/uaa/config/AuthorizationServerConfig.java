/**
 * Created on: Jun 23, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.config;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.approval.JdbcApprovalStore;
import org.springframework.security.oauth2.provider.code.JdbcAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.util.StreamUtils;

import com.cpa.uaa.account.Account;
import com.cpa.uaa.account.AccountRepository;

/**
 * @author Hieu Nguyen
 *
 */
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {
  
  private static final String OAUTH_PROVIDER = "CalgaryParkingAuthority";

  @Autowired
  private DataSource dataSource;

  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  private AccountRepository accountRepository;
  
  @Autowired
  private PasswordEncoder passwordEncoder;

  /* (non-Javadoc)
   * @see org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter#configure(org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer)
   */
  @Override
  public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
    clients.jdbc(dataSource).passwordEncoder(passwordEncoder);
  }

  @Override
  public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
    TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
    tokenEnhancerChain.setTokenEnhancers(Arrays.asList(userInfoTokenEnhancer(), accessTokenConverter()));

    endpoints
        .authenticationManager(authenticationManager)       
        .authorizationCodeServices(new JdbcAuthorizationCodeServices(dataSource)) //store authorization code in database
        .tokenStore(new JdbcTokenStore(dataSource)).tokenEnhancer(tokenEnhancerChain) //store access token in database
        .approvalStore(new JdbcApprovalStore(dataSource)); //store approval in database
  }

  /* (non-Javadoc)
   * @see org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter#configure(org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer)
   */
  @Override
  public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
    security  
      .checkTokenAccess("isAuthenticated()")      
      ;
  }

  /**
   * Encrypt the token string with a private key before sending it out
   * 
   * @return
   */
  @Bean
  public JwtAccessTokenConverter accessTokenConverter() {
    JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
    Resource resource = new ClassPathResource("cpa-jwt-private.jks");
    String privateKey = null;
    try (InputStream io = resource.getInputStream()) {
      privateKey = StreamUtils.copyToString(io, StandardCharsets.ISO_8859_1);
    }
    catch (final IOException e) {
      throw new RuntimeException(e);
    }
    converter.setSigningKey(privateKey);
    return converter;
  }

  /**
   * Add additional information to jwt token
   * 
   * @return
   */
  @Bean
  public TokenEnhancer userInfoTokenEnhancer() {
    return new TokenEnhancer() {
      @Override
      public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        final Account account = accountRepository.findAccountByIdOrUsername(authentication.getName());
        Map<String, Object> additionalInfo = new HashMap<>();
        additionalInfo.put("firstName", account.getFirstName());
        additionalInfo.put("lastName", account.getLastName());
        additionalInfo.put("email", account.getUsername());
        additionalInfo.put("provider", OAUTH_PROVIDER);
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
        return accessToken;
      }
    };
  }
}
