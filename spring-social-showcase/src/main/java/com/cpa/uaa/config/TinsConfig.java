/**
 * Created on: Jul 25, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.config;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.cpa.tins.client.MessageWebServiceImplPortType;

/**
 * @author Hieu Nguyen
 *
 */
@Configuration
public class TinsConfig {

  @Bean
  public MessageWebServiceImplPortType tinsClientService(@Value("${mail.tins.service.url}") final String tinsServiceUri) {
    final JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
    jaxWsProxyFactoryBean.setServiceClass(MessageWebServiceImplPortType.class);
    jaxWsProxyFactoryBean.setAddress(tinsServiceUri);
    return (MessageWebServiceImplPortType) jaxWsProxyFactoryBean.create();
  }
}
