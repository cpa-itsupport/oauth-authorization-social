/**
 * Created on: Jun 23, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.config;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.social.config.annotation.SocialConfigurerAdapter;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.jdbc.JdbcUsersConnectionRepository;
import org.springframework.social.connect.web.ProviderSignInUtils;

/**
 * @author Hieu Nguyen
 *
 */
@Configuration
public class SocialConfig extends SocialConfigurerAdapter {

  private static final Logger logger = LoggerFactory.getLogger(SocialConfig.class);

  @Autowired
  private DataSource dataSource;

  @Autowired
  private TextEncryptor textEncryptor;

  /* (non-Javadoc)
   * @see org.springframework.social.config.annotation.SocialConfigurerAdapter#getUsersConnectionRepository(org.springframework.social.connect.ConnectionFactoryLocator)
   */
  @Override
  @Bean
  @Primary
  public UsersConnectionRepository getUsersConnectionRepository(
      ConnectionFactoryLocator connectionFactoryLocator) {

    return new JdbcUsersConnectionRepository(dataSource, connectionFactoryLocator, textEncryptor);
  }

  @Bean
  public ProviderSignInUtils getProviderSignInUtils(UsersConnectionRepository usersConnectionRepository,
      ConnectionFactoryLocator connectionFactoryLocator) {
    return new ProviderSignInUtils(connectionFactoryLocator, usersConnectionRepository);
  }
}
