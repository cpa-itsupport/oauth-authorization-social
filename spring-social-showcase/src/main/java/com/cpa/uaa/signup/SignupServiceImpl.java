/**
 * Created on: Jul 13, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.signup;

import java.util.Locale;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cpa.tins.client.ArrayOfKeyValuePair;
import com.cpa.tins.client.Exception_Exception;
import com.cpa.tins.client.MailParamsBuilder;
import com.cpa.tins.client.MessageWebServiceImplPortType;
import com.cpa.uaa.account.Account;
import com.cpa.uaa.account.AccountService;

/**
 * @author Hieu Nguyen
 *
 */
@Service
@Transactional
public class SignupServiceImpl implements SignupService {

  private static final Logger logger = LoggerFactory.getLogger(SignupServiceImpl.class);

  final MessageSource messages;

  final String url;

  final MessageWebServiceImplPortType tinsServiceClient;

  final AccountService accountService;

  /**
   * 
   * @param mailSender
   * @param messages
   * @param accountService
   * @param tinsServiceClient
   * @param url
   */
  @Inject
  public SignupServiceImpl(MessageSource messages, AccountService accountService,
      MessageWebServiceImplPortType tinsServiceClient, @Value("${mail.account.activation.url}") String url) {
    this.messages = messages;
    this.url = url.trim();
    this.tinsServiceClient = tinsServiceClient;
    this.accountService = accountService;
  }

  /* (non-Javadoc)
   * @see com.cpa.uaa.signup.SignupService#sendVerificationMail(java.lang.String, java.lang.String)
   */
  @Override
  public void sendVerificationMail(final String email, final String token) {
    final Account account = accountService.findByUsernameOrId(email);
    //@formatter:off
    final ArrayOfKeyValuePair mailParams = new MailParamsBuilder()
        .from(messages.getMessage("mail.support.address", null, Locale.getDefault()))
        .to(email)
        .withStringParam("user", account.getFullName())
        .withStringParam("signup_confirm_url", url + token)
        .withSubject(messages.getMessage("mail.account.activation.subject", null, Locale.getDefault())).build();
    //@formatter:on
    try {
      logger.debug("Calling TINS to send email verification to [{}]", email);
      final Long sendEmail = tinsServiceClient.sendEmail("EPS", "CGY_SIGNUP_EMAIL_VERIFICATION",
          "CGY_SIGNUP_EMAIL_VERIFICATION", mailParams);
    }
    catch (Exception_Exception e) {
      logger.error("Unable to send email using TINS", e);
    }
  }
}
