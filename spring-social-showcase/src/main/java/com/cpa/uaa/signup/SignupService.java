/**
 * Created on: Jul 13, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.signup;

/**
 * @author Hieu Nguyen
 *
 */
public interface SignupService {

  void sendVerificationMail(String email, String token);

}