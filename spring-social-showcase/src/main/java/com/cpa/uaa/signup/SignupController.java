/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cpa.uaa.signup;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.social.connect.Connection;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.WebRequest;

import com.cpa.uaa.account.Account;
import com.cpa.uaa.account.AccountRepository;
import com.cpa.uaa.account.AccountService;
import com.cpa.uaa.account.UsernameAlreadyInUseException;
import com.cpa.uaa.controller.ResolveClientParamController;
import com.cpa.uaa.message.Message;
import com.cpa.uaa.message.MessageType;
import com.cpa.uaa.security.forgetpassword.VerificationToken;
import com.cpa.uaa.security.forgetpassword.VerificationTokenService;

@Controller
public class SignupController extends ResolveClientParamController {

  private static final Logger logger = LoggerFactory.getLogger(SignupController.class);

  private final AccountRepository accountRepository;

  private final AccountService accountService;

  private final VerificationTokenService verificationTokenService;

  private final SignupService signupService;

  /**
   * @param requestCache
   * @param accountRepository
   * @param accountService
   * @param requestCache2
   * @param verificationTokenService
   * @param signupService
   */
  public SignupController(RequestCache requestCache, AccountRepository accountRepository,
      AccountService accountService, VerificationTokenService verificationTokenService,
      SignupService signupService) {
    super(requestCache);
    this.accountRepository = accountRepository;
    this.accountService = accountService;
    this.verificationTokenService = verificationTokenService;
    this.signupService = signupService;
  }

  @GetMapping(value = "/signup")
  public SignupForm signupForm(WebRequest request) {
    Connection<?> connection = accountService.getSocialConnection(request);
    if (connection != null) {
      request.setAttribute("message",
          new Message(MessageType.INFO,
              "Your " + StringUtils.capitalize(connection.getKey().getProviderId())
                  + " account is not associated with a CPA account. Please enter a password to create CPA account."),
          WebRequest.SCOPE_REQUEST);
      return SignupForm.fromProviderUser(connection.fetchUserProfile());
    }
    else {
      return new SignupForm();
    }
  }

  @PostMapping(value = "/signup")
  public String signup(@Valid SignupForm form, BindingResult formBinding, NativeWebRequest nwRequest) {

    if (formBinding.hasErrors()) {
      return null;
    }
    Account account = createAccount(form, formBinding);

    if (account != null) {
      logger.debug("New account created for email [{}]", account.getUsername());
      accountService.sigInWithNoValidation(account.getId(), nwRequest);
      final Map<String, String> clientParams = resolveClientParams(nwRequest);
      final VerificationToken verificationToken = new VerificationToken(account.getId(),
          clientParams.get("client_id"), clientParams.get("redirect_uri"));
      verificationTokenService.createVerificationToken(verificationToken);
      signupService.sendVerificationMail(account.getUsername(), verificationToken.getToken());

      // logout before the redirect to destroy the cookie/session.
      new SecurityContextLogoutHandler().logout(nwRequest.getNativeRequest(HttpServletRequest.class),
          nwRequest.getNativeResponse(HttpServletResponse.class), null);

      return "redirect:/signin?info=email_verification";
    }
    else
      return null;
  }

  @GetMapping(value = "/signup/complete")
  public String completeSignup(WebRequest request, @RequestParam(required = true)
  final String token) {
    try {
      final VerificationToken validateToken = verificationTokenService.validateToken(token);
      accountRepository.toggleAccountStatus(validateToken.getUserId(), true);
      return verificationTokenService.getRedirectUri(token)
          .orElse("forward:/signin?info=complete_verification");
    }
    catch (Exception e) {
      request.setAttribute("message", new Message(MessageType.ERROR, "Your token is invalid or expired."),
          WebRequest.SCOPE_REQUEST);
      return "forward:/signin";
    }
  }

  // internal helpers
  private Account createAccount(SignupForm form, BindingResult formBinding) {
    try {
      Account account = new Account(form.getUsername(), form.getPassword(), form.getFirstName(),
          form.getLastName(), form.getEnabled());
      accountRepository.createAccount(account);
      return account;
    }
    catch (UsernameAlreadyInUseException e) {
      formBinding.rejectValue("username", "user.duplicateUsername", "Email address is already in use.");
      return null;
    }
  }

}
