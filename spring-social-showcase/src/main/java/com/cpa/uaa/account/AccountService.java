/**
 * Created on: Jul 17, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.account;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.connect.Connection;
import org.springframework.web.context.request.RequestAttributes;

/**
 * @author Hieu Nguyen
 *
 */
public interface AccountService {

  void sigInWithNoValidation(String userId, RequestAttributes requestAttributes);

  Connection<?> getSocialConnection(final RequestAttributes requestAttributes);

  /**
   * 
   * @param editProfileForm
   * @return true if username/email has been changed, false otherwise
   */
  boolean updateProfile(final EditProfileForm editProfileForm);

  Account getCurrentAccount();

  String getCurrentUserId();

  void toggleAccountStatus(final boolean isEnabled);

  Account findByUsernameOrId(final String usernameOrId);

  Authentication getCurrentAuthentication();

  int updatePassword(final String userId, final String newPassword);

}