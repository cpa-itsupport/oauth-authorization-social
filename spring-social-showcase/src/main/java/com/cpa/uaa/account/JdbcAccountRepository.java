/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cpa.uaa.account;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.inject.Inject;

import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cpa.uaa.signin.SignInDetails;

@Repository
public class JdbcAccountRepository implements AccountRepository {

  private final JdbcTemplate jdbcTemplate;

  private final PasswordEncoder passwordEncoder;

  @Inject
  public JdbcAccountRepository(JdbcTemplate jdbcTemplate, PasswordEncoder passwordEncoder) {
    this.jdbcTemplate = jdbcTemplate;
    this.passwordEncoder = passwordEncoder;
  }

  @Transactional
  public void createAccount(Account user) throws UsernameAlreadyInUseException {
    try {
      jdbcTemplate.update(
          "insert into Account (id, firstName, lastName, username, password, enabled) values (?, ?, ?, ?, ?, ?)",
          user.getId(), user.getFirstName().toUpperCase(), user.getLastName().toUpperCase(),
          user.getUsername().trim().toLowerCase(), passwordEncoder.encode(user.getPassword()),
          user.isEnabled());
    }
    catch (DuplicateKeyException e) {
      throw new UsernameAlreadyInUseException(user.getUsername());
    }
  }

  public Account findAccountByIdOrUsername(String idOrUsername) {
    return jdbcTemplate.queryForObject(
        "select id, username, password, firstName, lastName, enabled from Account where id = ? or username = ?",
        new RowMapper<Account>() {
          public Account mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Account(rs.getString("id"), rs.getString("username"), rs.getString("password"),
                rs.getString("firstName"), rs.getString("lastName"), rs.getBoolean("enabled"));
          }
        }, idOrUsername, idOrUsername);
  }

  @Override
  @Transactional
  public int updatePassword(final String userId, final String plainPassword) {
    return jdbcTemplate.update("update Account set password = ? where id = ?",
        passwordEncoder.encode(plainPassword), userId);
  }

  /* (non-Javadoc)
   * @see com.cpa.uaa.account.AccountRepository#enableAccount(java.lang.String)
   */
  @Override
  public int toggleAccountStatus(String userId, boolean isEnabled) {
    return jdbcTemplate.update("update Account set enabled = ? where id = ?", isEnabled, userId);
  }

  @Override
  public int updateAccount(final Account account) {
    return jdbcTemplate.update("update Account set username = ?, firstName = ?, lastName = ? where id = ?",
        account.getUsername().toLowerCase(), account.getFirstName().toUpperCase(),
        account.getLastName().toUpperCase(), account.getId());
  }

  @Override
  public int updateSigninDetails(final SignInDetails signInDetails) {
    return jdbcTemplate.update("update Account set last_signin_dt = ?, last_signin_ip = ? where id = ?",
        Date.from(signInDetails.getSignInDateTime().toInstant()), signInDetails.getSignInFromIp(),
        signInDetails.getUserId());
  }

}
