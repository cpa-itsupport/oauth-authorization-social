/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cpa.uaa.account;

import java.util.UUID;

public class Account {

  private final String id;

  private final String username;

  private final String password;

  private final String firstName;

  private final String lastName;

  private final Boolean enabled;

  public Account(String id, String username, String password, String firstName, String lastName,
      Boolean enabled) {
    this.id = id;
    this.username = username != null ? username.trim().toLowerCase() : "";
    this.password = password != null ? password.trim() : "";
    this.firstName = firstName != null ? firstName.trim().toUpperCase() : "";
    this.lastName = lastName != null ? lastName.trim().toUpperCase() : "";
    this.enabled = enabled;
  }

  public Account(String username, String password, String firstName, String lastName, Boolean enabled) {
    this(UUID.randomUUID().toString(), username, password, firstName, lastName, enabled);
  }

  /**
   * @return the enabled
   */
  public Boolean isEnabled() {
    return enabled;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getId() {
    return id;
  }

  public String getFullName() {
    return firstName + " " + lastName;
  }
}
