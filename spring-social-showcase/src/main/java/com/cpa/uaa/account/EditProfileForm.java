/**
 * Created on: Jul 14, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.account;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.util.StringUtils;

/**
 * @author Hieu Nguyen
 *
 */
public class EditProfileForm {

  @NotEmpty
  @Email
  private String email;

  @NotEmpty
  private String firstName;

  @NotEmpty
  private String lastName;

  private boolean redirectUriEnabled = false;

  /**
   * 
   */
  public EditProfileForm() {
  }

  /**
   * @param email
   * @param firstName
   * @param lastName
   */
  public EditProfileForm(String email, String firstName, String lastName) {
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
  }

  /**
   * @return the redirectUriEnabled
   */
  public boolean isRedirectUriEnabled() {
    return redirectUriEnabled;
  }

  /**
   * @param redirectUriEnabled the redirectUriEnabled to set
   */
  public void setRedirectUriEnabled(boolean redirectUriEnabled) {
    this.redirectUriEnabled = redirectUriEnabled;
  }

  /**
   * @return the email
   */
  public String getEmail() {
    return email;
  }

  /**
   * @param email the email to set
   */
  public void setEmail(String email) {
    if (StringUtils.hasText(email))
      this.email = email.toLowerCase();
  }

  /**
   * @return the firstName
   */
  public String getFirstName() {
    return firstName;
  }

  /**
   * @param firstName the firstName to set
   */
  public void setFirstName(String firstName) {
    if (StringUtils.hasText(firstName))
      this.firstName = firstName.toUpperCase();
  }

  /**
   * @return the lastName
   */
  public String getLastName() {
    return lastName;
  }

  /**
   * @param lastName the lastName to set
   */
  public void setLastName(String lastName) {
    if (StringUtils.hasText(lastName))
      this.lastName = lastName.toUpperCase();
  }
}
