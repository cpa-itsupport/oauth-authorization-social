/**
 * Created on: Jul 17, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.account;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestAttributes;

import com.cpa.uaa.security.forgetpassword.VerificationTokenService;
import com.cpa.uaa.signin.LocalAccountSignInService;

/**
 * @author Hieu Nguyen
 *
 */
@Service
@Transactional
public class AccountServiceImpl implements AccountService {

  private static final Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);

  final LocalAccountSignInService localAccountSignInService;

  final ProviderSignInUtils providerSignInUtils;

  final AccountRepository accountRepository;

  final VerificationTokenService verificationTokenService;

  /**
   * @param localAccountSignInService
   * @param providerSignInUtils
   * @param accountRepository
   * @param verificationTokenService
   */
  public AccountServiceImpl(LocalAccountSignInService localAccountSignInService,
      ProviderSignInUtils providerSignInUtils, AccountRepository accountRepository,
      VerificationTokenService verificationTokenService) {
    this.localAccountSignInService = localAccountSignInService;
    this.providerSignInUtils = providerSignInUtils;
    this.accountRepository = accountRepository;
    this.verificationTokenService = verificationTokenService;
  }

  /* (non-Javadoc)
   * @see com.cpa.uaa.account.AccountService#sigInWithNoValidation(java.lang.String, org.springframework.web.context.request.RequestAttributes)
   */
  @Override
  public void sigInWithNoValidation(final String userId, final RequestAttributes requestAttributes) {
    localAccountSignInService.signinNoValidation(userId);
    providerSignInUtils.doPostSignUp(userId, requestAttributes);
  }

  @Override
  public Connection<?> getSocialConnection(final RequestAttributes requestAttributes) {
    return providerSignInUtils.getConnectionFromSession(requestAttributes);
  }

  @Override
  public boolean updateProfile(final EditProfileForm editProfileForm) {
    final Account account = getCurrentAccount();

    final Account newProfile = new Account(account.getId(), editProfileForm.getEmail(), account.getPassword(),
        editProfileForm.getFirstName(), editProfileForm.getLastName(), account.isEnabled());
    accountRepository.updateAccount(newProfile);

    final boolean emailChanged = !account.getUsername().equals(editProfileForm.getEmail());

    if (emailChanged) {
      logger.info("Changing email from [{}] to [{}]", account.getUsername(), editProfileForm.getEmail());
      this.toggleAccountStatus(false);
    }

    return emailChanged;
  }

  @Override
  public Account getCurrentAccount() {
    final User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    return accountRepository.findAccountByIdOrUsername(user.getUsername());
  }

  @Override
  public String getCurrentUserId() {
    final User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    return user.getUsername();
  }

  @Override
  public Authentication getCurrentAuthentication() {
    return SecurityContextHolder.getContext().getAuthentication();
  }

  @Override
  public void toggleAccountStatus(final boolean isEnabled) {
    accountRepository.toggleAccountStatus(getCurrentUserId(), isEnabled);
  }

  @Override
  public Account findByUsernameOrId(final String usernameOrId) {
    return accountRepository.findAccountByIdOrUsername(usernameOrId);
  }

  @Override
  public int updatePassword(final String userId, final String newPassword) {
    return accountRepository.updatePassword(userId, newPassword);
  }

}
