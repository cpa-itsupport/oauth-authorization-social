package com.cpa.uaa.account;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.core.annotation.Order;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.cpa.uaa.message.Message;
import com.cpa.uaa.message.MessageType;
import com.cpa.uaa.security.forgetpassword.VerificationToken;
import com.cpa.uaa.security.forgetpassword.VerificationTokenService;
import com.cpa.uaa.signup.SignupService;
import com.cpa.uaa.web.WebUtilsService;
import com.cpa.uaa.web.WebUtilsServiceImpl;

/**
 * @author Hieu Nguyen
 *
 */
@Controller
@Order(10)
public class AccountController {

  final AccountService accountService;

  final VerificationTokenService verificationTokenService;

  final WebUtilsService webUtilsService;

  final SignupService signupService;

  /**
   * @param accountService
   * @param verificationTokenService
   * @param webUtilsService
   * @param signupService
   */
  public AccountController(AccountService accountService, VerificationTokenService verificationTokenService,
      WebUtilsService webUtilsService, SignupService signupService) {
    this.accountService = accountService;
    this.verificationTokenService = verificationTokenService;
    this.webUtilsService = webUtilsService;
    this.signupService = signupService;
  }

  @GetMapping("/editProfile")
  public EditProfileForm getEditProfileForm(final HttpServletRequest hsRequest,
      final HttpServletResponse hsResponse) {
    final Account account = this.accountService.getCurrentAccount();

    final EditProfileForm profileForm = new EditProfileForm(account.getUsername(), account.getFirstName(),
        account.getLastName());

    final String redirectUri = hsRequest.getParameter(WebUtilsServiceImpl.REDIRECT_URI);

    if (StringUtils.hasText(redirectUri)) {
      webUtilsService.getRequestCache().saveRequest(hsRequest, hsResponse);
      profileForm.setRedirectUriEnabled(true);
    }

    return profileForm;
  }

  @PostMapping(value = "/editProfile", params = "action=save")
  public String editProfile(@Valid
  final EditProfileForm profileForm, final BindingResult bindingResult, final NativeWebRequest nwRequest) {

    final HttpServletRequest hsRequest = nwRequest.getNativeRequest(HttpServletRequest.class);
    final HttpServletResponse hsResponse = nwRequest.getNativeResponse(HttpServletResponse.class);

    webUtilsService.getSavedRequest(hsRequest, hsResponse).ifPresent(sr -> {
      profileForm.setRedirectUriEnabled(true);
    });

    if (bindingResult.hasErrors()) {
      return null;
    }

    boolean emailChanged = false;

    try {
      emailChanged = accountService.updateProfile(profileForm);
    }
    catch (DuplicateKeyException e) {
      bindingResult.rejectValue("email", "email.error.duplication", "Email already existed in the system.");
      return null;
    }

    if (emailChanged) {
      final Map<String, String> clientParams = webUtilsService.resolveClientParams(nwRequest);
      final Account currentAccount = accountService.getCurrentAccount();
      final VerificationToken verificationToken = new VerificationToken(currentAccount.getId(),
          clientParams.get("client_id"), clientParams.get("redirect_uri"));
      verificationTokenService.createVerificationToken(verificationToken);
      signupService.sendVerificationMail(profileForm.getEmail(), verificationToken.getToken());
      nwRequest.setAttribute("message",
          new Message(MessageType.WARNING,
              "Your email has been changed, please logout, check your email to activate it, and login with your new email as the username."),
          WebRequest.SCOPE_REQUEST);
    }
    else {
      nwRequest.setAttribute("message", new Message(MessageType.INFO, "Your profile has been updated."),
          WebRequest.SCOPE_REQUEST);
    }

    return null;
  }

  @PostMapping(value = "/editProfile", params = "action=goback")
  public String goBack(final HttpServletRequest hsRequest, final HttpServletResponse hsResponse) {
    final Optional<String> redirectUri = webUtilsService.getSavedRequest(hsRequest, hsResponse).map(sr -> {
      final UriComponents build = UriComponentsBuilder.fromHttpUrl(sr.getRedirectUrl()).build();
      String uri = build.getQueryParams().get(WebUtilsServiceImpl.REDIRECT_URI).get(0);
      uri = uri + "?token="
          + build.getQueryParams().getOrDefault(WebUtilsServiceImpl.TOKEN, Arrays.asList("")).get(0);
      return uri;
    });

    return "redirect:" + redirectUri.orElse("/editProfile");
  }

}
