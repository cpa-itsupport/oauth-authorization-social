/**
 * Created on: Jul 26, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.exit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Hieu Nguyen
 *
 */
@Controller
public class ExitController {

  @GetMapping(value = "/exit")
  public String exit(final HttpServletRequest request, final HttpServletResponse response,
      @RequestParam(name = "redirect_uri") String redirecUri) {

    new SecurityContextLogoutHandler().logout(request, response, null);

    return "redirect:" + redirecUri;
  }
}
