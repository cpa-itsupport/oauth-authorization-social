/**
 * Created on: Jul 17, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.web;

import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.web.context.request.NativeWebRequest;

/**
 * @author Hieu Nguyen
 *
 */
public interface WebUtilsService {

  Map<String, String> resolveClientParams(NativeWebRequest nwRequest);

  boolean hasRedirectUriInformation(final NativeWebRequest nwRequest);

  RequestCache getRequestCache();

  Optional<SavedRequest> getSavedRequest(final HttpServletRequest hsRequest, final HttpServletResponse hsResponse);

  void removeSavedRequest(final HttpServletRequest hsRequest, final HttpServletResponse hsResponse);

  String getClientIp(final HttpServletRequest hsRequest);

  String getClientIpFromNativeWebRequest(final NativeWebRequest nwRequest);

}