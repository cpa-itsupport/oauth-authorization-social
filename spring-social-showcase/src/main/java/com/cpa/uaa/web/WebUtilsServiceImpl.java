/**
 * Created on: Jul 17, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.web;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.savedrequest.DefaultSavedRequest;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.NativeWebRequest;

import com.cpa.uaa.account.AccountService;

/**
 * @author Hieu Nguyen
 *
 */
@Service
public class WebUtilsServiceImpl implements WebUtilsService {

  final RequestCache requestCache;

  final AccountService accountService;

  public static final String CLIENT_ID = "client_id";

  public static final String REDIRECT_URI = "redirect_uri";

  public static final String TOKEN = "token";

  /**
   * @param requestCache
   * @param accountService
   */
  @Inject
  public WebUtilsServiceImpl(RequestCache requestCache, AccountService accountService) {
    this.requestCache = requestCache;
    this.accountService = accountService;
  }

  /* (non-Javadoc)
   * @see com.cpa.uaa.web.WebUtilsService#resolveClientParams(org.springframework.web.context.request.NativeWebRequest)
   */
  @Override
  public Map<String, String> resolveClientParams(final NativeWebRequest nwRequest) {
    Map<String, String> maps = new HashMap<String, String>();

    HttpServletRequest httpReq = nwRequest.getNativeRequest(HttpServletRequest.class);
    HttpServletResponse httpRes = nwRequest.getNativeResponse(HttpServletResponse.class);
    final DefaultSavedRequest defaultSavedRequest = (DefaultSavedRequest) requestCache.getRequest(httpReq,
        httpRes);

    if (defaultSavedRequest != null) {
      final String[] clientIds = defaultSavedRequest.getParameterValues(CLIENT_ID);
      final String[] redirectUris = defaultSavedRequest.getParameterValues(REDIRECT_URI);
      final String[] tokens = defaultSavedRequest.getParameterValues(TOKEN);

      if (clientIds != null && clientIds.length > 0)
        maps.put(CLIENT_ID, clientIds[0]);
      else
        maps.put(CLIENT_ID, null);

      if (redirectUris != null && redirectUris.length > 0)
        maps.put(REDIRECT_URI, redirectUris[0]);
      else
        maps.put(REDIRECT_URI, null);

      if (tokens != null && tokens.length > 0)
        maps.put(TOKEN, tokens[0]);
      else
        maps.put(TOKEN, null);
    }

    return maps;
  }

  @Override
  public boolean hasRedirectUriInformation(final NativeWebRequest nwRequest) {
    final Map<String, String> clientParams = this.resolveClientParams(nwRequest);

    return StringUtils.hasText(clientParams.get(REDIRECT_URI));
  }

  @Override
  public RequestCache getRequestCache() {
    return this.requestCache;
  }

  @Override
  public Optional<SavedRequest> getSavedRequest(final HttpServletRequest hsRequest,
      final HttpServletResponse hsResponse) {
    return Optional.ofNullable(requestCache.getRequest(hsRequest, hsResponse));
  }

  @Override
  public void removeSavedRequest(final HttpServletRequest hsRequest, final HttpServletResponse hsResponse) {
    requestCache.removeRequest(hsRequest, hsResponse);
  }

  @Override
  public String getClientIp(final HttpServletRequest hsRequest) {
    String remoteAddr = "";

    if (hsRequest != null) {
      remoteAddr = hsRequest.getHeader("X-FORWARDED-FOR");
      if (remoteAddr == null || "".equals(remoteAddr)) {
        remoteAddr = hsRequest.getRemoteAddr();
      }
    }

    return remoteAddr;
  }

  @Override
  public String getClientIpFromNativeWebRequest(final NativeWebRequest nwRequest) {
    final HttpServletRequest hsRequest = nwRequest.getNativeRequest(HttpServletRequest.class);
    return this.getClientIp(hsRequest);
  }
}
