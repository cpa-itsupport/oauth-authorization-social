/**
 * Created on: Jul 13, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.signin;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

/**
 * @author Hieu Nguyen
 *
 */
public class AuthenticationExceptionErrorMessageAuthenticationFailureHandler
    implements AuthenticationFailureHandler {

  private static final Logger logger = LoggerFactory
      .getLogger(AuthenticationExceptionErrorMessageAuthenticationFailureHandler.class);

  final Map<Class<? extends AuthenticationException>, String> authenticationExceptionErrorMessages;

  /**
   * @param authenticationExceptionErrorMessages
   */
  public AuthenticationExceptionErrorMessageAuthenticationFailureHandler(
      Map<Class<? extends AuthenticationException>, String> authenticationExceptionErrorMessages) {
    this.authenticationExceptionErrorMessages = authenticationExceptionErrorMessages;
  }

  /* (non-Javadoc)
   * @see org.springframework.security.web.authentication.AuthenticationFailureHandler#onAuthenticationFailure(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.springframework.security.core.AuthenticationException)
   */
  @Override
  public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
      AuthenticationException exception) throws IOException, ServletException {
    final String errorCode;
    logger.info("exception class [{}]", exception.getClass());
    if (authenticationExceptionErrorMessages.containsKey(exception.getClass())) {
      errorCode = authenticationExceptionErrorMessages.get(exception.getClass());
    }
    else
      errorCode = "bad_credentials";

    new DefaultRedirectStrategy().sendRedirect(request, response, "/signin?error=" + errorCode);
  }

}
