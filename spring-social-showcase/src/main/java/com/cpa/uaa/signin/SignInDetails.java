/**
 * Created on: Aug 2, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.signin;

import java.time.ZonedDateTime;

import org.springframework.util.Assert;

/**
 * @author Hieu Nguyen
 *
 */
public class SignInDetails {

  private final ZonedDateTime signInDateTime;

  private final String signInFromIp;

  private final String userId;

  /**
   * @param signInDateTime
   * @param signInFromIp
   * @param userId
   */
  public SignInDetails(ZonedDateTime signInDateTime, String signInFromIp, String userId) {
    Assert.hasText(userId, "userId cannot be empty or null.");
    this.signInDateTime = signInDateTime;
    this.signInFromIp = signInFromIp == null ? "" : signInFromIp.trim();
    this.userId = userId;
  }

  /**
   * @param signInFromIp
   * @param userId
   */
  public SignInDetails(String signInFromIp, String userId) {
    this(ZonedDateTime.now(), signInFromIp, userId);
  }

  /**
   * @return the signInDateTime
   */
  public ZonedDateTime getSignInDateTime() {
    return signInDateTime;
  }

  /**
   * @return the signInFromIp
   */
  public String getSignInFromIp() {
    return signInFromIp;
  }

  /**
   * @return the userId
   */
  public String getUserId() {
    return userId;
  }
}
