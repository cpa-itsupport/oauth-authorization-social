/**
 * Created on: Jul 17, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.signin;

import org.springframework.security.core.userdetails.User;

/**
 * @author Hieu Nguyen
 *
 */
public interface LocalAccountSignInService {

  /**
   * Programmatically signs in the user with the given the user ID.
   */
  User signinWithValidation(final String userId);

  /**
   * Programmatically signs in the user with the given the user ID and token
   */
  void signInWithTokenCredential(final String userId, final String token);

  void signinNoValidation(final String userId);

  void updateSigninDetails(final SignInDetails signInDetails);

}