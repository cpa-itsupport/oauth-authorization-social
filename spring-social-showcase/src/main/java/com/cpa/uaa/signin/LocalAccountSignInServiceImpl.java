/**
 * Created on: Jul 17, 2017
 * @author: cpahmn
 */
package com.cpa.uaa.signin;

import java.util.Arrays;

import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cpa.uaa.account.Account;
import com.cpa.uaa.account.AccountRepository;

/**
 * @author Hieu Nguyen
 *
 */
@Service
@Transactional
public class LocalAccountSignInServiceImpl implements LocalAccountSignInService {

  final AccountRepository accountRepository;

  /**
   * @param accountRepository
   */
  public LocalAccountSignInServiceImpl(AccountRepository accountRepository) {
    this.accountRepository = accountRepository;
  }

  /* (non-Javadoc)
   * @see com.cpa.uaa.signin.LocalAccountSignInService#signin(java.lang.String)
   */
  @Override
  public User signinWithValidation(final String userId) {
    final Account account = accountRepository.findAccountByIdOrUsername(userId);

    if (!account.isEnabled()) {
      throw new DisabledException("Account is disabled.");
    }
    else {
      final User user = new User(account.getId(), account.getPassword(), account.isEnabled(), true, true,
          true, Arrays.asList(new SimpleGrantedAuthority("ROLE_USER")));

      SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(user,
          user.getPassword(), Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"))));

      return user;
    }

  }

  /* (non-Javadoc)
   * @see com.cpa.uaa.signin.LocalAccountSignInService#signin(java.lang.String)
   */
  @Override
  public void signinNoValidation(final String userId) {
    SecurityContextHolder.getContext()
        .setAuthentication(new UsernamePasswordAuthenticationToken(userId, null, null));
  }

  /* (non-Javadoc)
   * @see com.cpa.uaa.signin.LocalAccountSignInService#signinWithToken(java.lang.String, java.lang.String)
   */
  @Override
  public void signInWithTokenCredential(final String userId, final String token) {
    final Account account = accountRepository.findAccountByIdOrUsername(userId);
    final User user = new User(account.getId(), account.getPassword(), account.isEnabled(), true, true, true,
        Arrays.asList(new SimpleGrantedAuthority("ROLE_USER")));

    SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(user, token,
        Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"))));
  }

  @Override
  public void updateSigninDetails(final SignInDetails signInDetails) {
    accountRepository.updateSigninDetails(signInDetails);
  }

}
