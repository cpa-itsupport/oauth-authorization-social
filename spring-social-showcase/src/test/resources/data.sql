Insert into ACCOUNT (id,username,firstname,lastname,password,enabled,createddt) values ('ac7d83f7-13de-438a-ba1d-9cc4c283a669','test1@test.com','TEST1','TESTER','AAAAoAAB9AClZduPHeuerKMHnz9YDFXfMcbzWDgtszSW2llC','false'  ,timestamp '2017-07-27 13:30:41.835362');
Insert into ACCOUNT (id,username,firstname,lastname,password,enabled,createddt) values ('12a88495-fcaa-457f-8b7d-cd3ac9d349b7','test2@test.com','TEST2','TESTER','AAAAoAAB9AClZduPHeuerKMHnz9YDFXfMcbzWDgtszSW2llC','true'  ,timestamp '2017-07-27 13:30:41.835362');

Insert into OAUTH_CLIENT_DETAILS (client_id,resource_ids,client_secret,scope,authorized_grant_types,web_server_redirect_uri,authorities,access_token_validity,refresh_token_validity,additional_information,autoapprove) values ('test-client','test-resource','testtest','read,write','implicit','http://example.com','role_user',7200,0,null,'true');

insert into verification_token (user_id, token, expiry_at, client_id, redirect_uri) values ('ac7d83f7-13de-438a-ba1d-9cc4c283a669','0bc1d071-c473-4204-b5f1-71bbe222584e',timestamp '2088-07-27 13:30:41.835362','test-client','http://example.com');
insert into verification_token (user_id, token, expiry_at, client_id, redirect_uri) values ('ac7d83f7-13de-438a-ba1d-9cc4c283a669','801ad718-718f-4fc1-91a0-e37b43af1987',timestamp '2017-07-27 13:30:41.835362','test-client','http://example.com');

insert into verification_token (user_id, token, expiry_at, client_id, redirect_uri) values ('12a88495-fcaa-457f-8b7d-cd3ac9d349b7','1354cd54-c473-4204-b5f1-71bbe123584e',timestamp '2088-07-27 13:30:41.835362','test-client','http://example.com');

