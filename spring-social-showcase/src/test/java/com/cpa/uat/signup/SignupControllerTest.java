/**
 * Created on: Jul 27, 2017
 * @author: cpahmn
 */
package com.cpa.uat.signup;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import com.cpa.tins.client.MessageWebServiceImplPortType;
import com.cpa.uat.config.AbstractWebSecurityTest;

/**
 * @author Hieu Nguyen
 *
 */
public class SignupControllerTest extends AbstractWebSecurityTest {
  
  @Autowired
  protected MessageWebServiceImplPortType tinsClientService;

  @Test
  public void test_GET_SignUp() throws Exception {
    //@formatter:off
    super.mvc.perform(get("/signup"))
              .andExpect(status().isOk())              
              .andExpect(model().attribute("signupForm", hasProperty("firstName", isEmptyOrNullString())))
              .andExpect(model().attribute("signupForm", hasProperty("lastName", isEmptyOrNullString())))
              .andExpect(model().attribute("signupForm", hasProperty("username", isEmptyOrNullString())))
              .andExpect(model().attribute("signupForm", hasProperty("password", isEmptyOrNullString())));
              ;
    //@formatter:on
  }

  @Test
  public void test_POST_Signup_success() throws Exception {
    //@formatter:off
    super.mvc.perform(post("/signup")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .with(csrf())
                        .param("username", "test3@test.com")
                        .param("firstName", "Test3")
                        .param("lastName", "TESTER")
                        .param("password", "asdf12345")
                      )
                      .andExpect(status().is3xxRedirection())
                      .andExpect(unauthenticated())
                      .andExpect(view().name("redirect:/signin?info=email_verification"))
                      ;
    //@formatter:on
  }

  @Test
  public void test_POST_Signup_invalid_email() throws Exception {
    //@formatter:off
    super.mvc.perform(post("/signup")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .with(csrf())
                        .param("username", "test3test.com")
                        .param("firstName", "Test3")
                        .param("lastName", "TESTER")
                        .param("password", "asdf12345")
                      )
                      .andExpect(status().isOk())
                      .andExpect(view().name("signup"))
                      .andExpect(unauthenticated())
                      .andExpect(model().attributeHasFieldErrors("signupForm", "username"))
                      ;
    //@formatter:on
  }

  @Test
  public void test_POST_Signup_duplicated_username() throws Exception {
    //@formatter:off
    super.mvc.perform(post("/signup")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .with(csrf())
                        .param("username", "test2@test.com")
                        .param("firstName", "Test3")
                        .param("lastName", "TESTER")
                        .param("password", "asdf12345")
                      )
                      .andExpect(status().isOk())
                      .andExpect(view().name("signup"))
                      .andExpect(unauthenticated())
                      .andExpect(model().attributeHasFieldErrors("signupForm", "username"))
                      ;
    //@formatter:on
  }

  @Test
  public void test_GET_Signup_complete_invalid_token() throws Exception {
    //@formatter:off
    super.mvc.perform(get("/signup/complete")                        
                      )
                      .andExpect(status().isBadRequest())                               
                      ;
    
    super.mvc.perform(get("/signup/complete?token={token}", "invalid")                        
                      )
                      .andExpect(status().isOk())
                      .andExpect(forwardedUrl("/signin"))
                      .andExpect(unauthenticated())     
                      .andExpect(request().attribute("message", hasProperty("text", equalTo("Your token is invalid or expired."))))
                      ;
    
    super.mvc.perform(get("/signup/complete?token={token}", "801ad718-718f-4fc1-91a0-e37b43af1987")                        
                      )
                      .andExpect(status().isOk())
                      .andExpect(forwardedUrl("/signin"))
                      .andExpect(unauthenticated())     
                      .andExpect(request().attribute("message", hasProperty("text", equalTo("Your token is invalid or expired."))))
                      ;
    //@formatter:on
  }

  @Test
  public void test_GET_Signup_complete_valid_token() throws Exception {
    
    //@formatter:off
    super.mvc.perform(get("/signup/complete?token={token}", "0bc1d071-c473-4204-b5f1-71bbe222584e")                        
                      )
                      .andExpect(status().is3xxRedirection())
                      .andExpect(redirectedUrl("/oauth/authorize?response_type=token&client_id=test-client&redirect_uri=http://example.com"))
                      .andExpect(authenticated())     
                      ;
    //@formatter:on
  }
}
