/**
 * Created on: Aug 2, 2017
 * @author: cpahmn
 */
package com.cpa.uat.exit;

import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;

import com.cpa.uat.config.AbstractWebSecurityTest;

/**
 * @author Hieu Nguyen
 *
 */
public class ExitControllerTest extends AbstractWebSecurityTest {

  @Test
  @WithMockUser(username = TEST2_ID)
  public void test_GET_authorized_exit_success() throws Exception {
    //@formatter:off
    super.mvc.perform(get("/exit?redirect_uri=http://example.com"))
                      .andExpect(unauthenticated()) 
                      .andExpect(status().is3xxRedirection())
                      .andExpect(redirectedUrl("http://example.com"))
                      ;
    //@formatter:on
  }
  
  @Test
  public void test_GET_unauthorized_exit_success() throws Exception {
    //@formatter:off
    super.mvc.perform(get("/exit?redirect_uri=http://example.com"))
                      .andExpect(unauthenticated()) 
                      .andExpect(status().is3xxRedirection())
                      .andExpect(redirectedUrl("http://example.com"))
                      ;
    //@formatter:on
  }
}
