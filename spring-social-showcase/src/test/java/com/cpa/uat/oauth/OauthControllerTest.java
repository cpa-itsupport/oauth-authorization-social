/**
 * Created on: Jul 27, 2017
 * @author: cpahmn
 */
package com.cpa.uat.oauth;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrlPattern;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;

import com.cpa.uat.config.AbstractWebSecurityTest;

/**
 * @author Hieu Nguyen
 *
 */
public class OauthControllerTest extends AbstractWebSecurityTest {

  @Test
  public void testGetOauthAuthorize_with_no_authentication() throws Exception {
    super.mvc
        .perform(get("/oauth/authorize?response_type=token&client_id={clientId}&redirect_uri={redirectUri}",
            "test-client", "http://example.com"))
        .andExpect(redirectedUrl("http://localhost/signin"));
  }
  
  @Test
  @WithMockUser(username = TEST1_ID)
  public void testGetOauthAuthorize_with_preexisting_success_authentication() throws Exception {
    super.mvc
        .perform(get("/oauth/authorize?response_type=token&client_id={clientId}&redirect_uri={redirectUri}",
            "test-client", "http://example.com"))
        .andExpect(redirectedUrlPattern("http://example.com*/**"));
  }
  
  @Test
  @WithMockUser(username = TEST1_ID)
  public void testGetOauthAuthorize_with_preexisting_success_authentication_and_wrong_client_id() throws Exception {
    super.mvc
        .perform(get("/oauth/authorize?response_type=token&client_id={clientId}&redirect_uri={redirectUri}",
            "test-client1", "http://example.com"))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  @WithMockUser(username = TEST1_ID)
  public void testGetOauthAuthorize_with_preexisting_success_authentication_and_wrong_redirectUri() throws Exception {
    super.mvc
        .perform(get("/oauth/authorize?response_type=token&client_id={clientId}&redirect_uri={redirectUri}",
            "test-client", "http://example1.com"))
        .andExpect(status().is4xxClientError());
  }

}
