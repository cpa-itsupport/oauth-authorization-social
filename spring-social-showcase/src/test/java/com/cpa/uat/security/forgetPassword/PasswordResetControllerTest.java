/**
 * Created on: Jul 31, 2017
 * @author: cpahmn
 */
package com.cpa.uat.security.forgetPassword;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;

import com.cpa.uaa.message.MessageType;
import com.cpa.uat.config.AbstractWebSecurityTest;

/**
 * @author Hieu Nguyen
 *
 */
@Transactional
public class PasswordResetControllerTest extends AbstractWebSecurityTest {

  @Test
  public void test_GET_forgetpassword() throws Exception {
    //@formatter:off
    super.mvc.perform(get("/forgetPassword"))
                        .andExpect(unauthenticated())
                        .andExpect(status().isOk())
                        .andExpect(view().name("forgetPassword"))
                        .andExpect(request().attribute("email", isEmptyOrNullString()))
                        ;
    //@formatter:on
  }

  @Test
  public void test_POST_forgetpassword_errors() throws Exception {
    //@formatter:off
    super.mvc.perform(post("/forgetPassword?email={email}", "test1@test.com")
                          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                          .with(csrf()))
                      .andExpect(unauthenticated())
                      .andExpect(status().isOk())
                      .andExpect(view().name("forgetPassword"))        
                      .andExpect(request().attribute("message", hasProperty("text", equalTo("You cannot reset the password since your account has been disabled."))))
                      .andExpect(request().attribute("message", hasProperty("type", equalTo(MessageType.ERROR))))
                      ;
    
    super.mvc.perform(post("/forgetPassword?email={email}", "blahblah@test.com")
                          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                           .with(csrf()))
                      .andExpect(unauthenticated())
                      .andExpect(status().isOk())
                      .andExpect(view().name("forgetPassword"))        
                      .andExpect(request().attribute("message", hasProperty("text", equalTo("The username is invalid."))))
                      .andExpect(request().attribute("message", hasProperty("type", equalTo(MessageType.ERROR))))
    ;
    //@formatter:on
  }

  @Test
  public void test_POST_forgetpassword_success() throws Exception {
    //@formatter:off
    super.mvc.perform(post("/forgetPassword?email={email}", "test2@test.com")
                          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                          .with(csrf()))
                      .andExpect(unauthenticated())
                      .andExpect(status().isOk())
                      .andExpect(view().name("forgetPassword"))        
                      .andExpect(request().attribute("message", hasProperty("text", equalTo("An email has been sent to you, please check your email in a few minutes."))))
                      .andExpect(request().attribute("message", hasProperty("type", equalTo(MessageType.INFO))))
                      ;
    
    //@formatter:on
  }

  @Test
  public void test_GET_resetPassword_invalid_empty_token() throws Exception {
    //@formatter:off
    super.mvc.perform(get("/resetPassword")
                          .contentType(MediaType.APPLICATION_FORM_URLENCODED))
                      .andExpect(unauthenticated())
                      .andExpect(status().is4xxClientError())                                                   
                      ;
    
    super.mvc.perform(get("/resetPassword?token={token}","invalid")
                          .contentType(MediaType.APPLICATION_FORM_URLENCODED))
                      .andExpect(unauthenticated())
                      .andExpect(status().isOk())
                      .andExpect(forwardedUrl("/signin"))
                      .andExpect(request().attribute("message", hasProperty("text", equalTo("Invalid token to update your password."))))
                      .andExpect(request().attribute("message", hasProperty("type", equalTo(MessageType.ERROR))))
                      ;
    
    //@formatter:on
  }

  @Test
  public void test_GET_resetPassword_valid_token() throws Exception {
    //@formatter:off
    super.mvc.perform(get("/resetPassword?token={token}","1354cd54-c473-4204-b5f1-71bbe123584e")
                          .contentType(MediaType.APPLICATION_FORM_URLENCODED))
                      .andExpect(authenticated()) //user should be authenticated if the token is valid.
                      .andExpect(status().isOk())
                      .andExpect(forwardedUrl("/updatePassword"))
                      ;
    
    //@formatter:on
  }

  @Test
  @WithMockUser(username = TEST2_ID, password = "1354cd54-c473-4204-b5f1-71bbe123584e")
  public void test_POST_updatePassword_error() throws Exception {
    //@formatter:off
    super.mvc.perform(post("/updatePassword")
                          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                          .with(csrf())
                          .param("newPassword", "test1") //password too short
                          .param("confirmPassword", "testtesttest"))
                      .andExpect(authenticated()) 
                      .andExpect(status().isOk())
                      .andExpect(view().name("updatePassword"))
                      .andExpect(model().attributeHasFieldErrors("updatePasswordForm", "newPassword"))
                      ;
    
    super.mvc.perform(post("/updatePassword")
                          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                          .with(csrf())
                          .param("newPassword", "test1test")
                          .param("confirmPassword", "testtesttest")) //passwords don't match
                      .andExpect(authenticated()) 
                      .andExpect(status().isOk())
                      .andExpect(view().name("updatePassword"))
                      .andExpect(model().attributeHasFieldErrors("updatePasswordForm", "confirmPassword"))
                      ;
    //@formatter:on
  }

  @Test
  @WithMockUser(username = TEST2_ID, password = "1354cd54-c473-4204-b5f1-71bbe123584e")
  public void test_POST_updatePassword_success() throws Exception {
    //@formatter:off
    super.mvc.perform(post("/updatePassword")
                          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                          .with(csrf())
                          .param("newPassword", "testtesttest") 
                          .param("confirmPassword", "testtesttest"))
                      .andExpect(authenticated()) 
                      .andExpect(status().is3xxRedirection())
                      .andExpect(redirectedUrl("/oauth/authorize?response_type=token&client_id=test-client&redirect_uri=http://example.com"))
                      .andExpect(request().attribute("message", hasProperty("text", equalTo("Your password has been changed successfully."))))
                      .andExpect(request().attribute("message", hasProperty("type", equalTo(MessageType.INFO))))
                      ;
    //@formatter:on
  }
  
  @Test
  @WithMockUser(username = TEST2_ID)
  public void test_GET_changePassword() throws Exception {
    //@formatter:off
    super.mvc.perform(get("/changePassword"))
                        .andExpect(authenticated())
                        .andExpect(status().isOk())
                        .andExpect(request().sessionAttribute("SPRING_SECURITY_SAVED_REQUEST", nullValue()))
                        .andExpect(model().attribute("updatePasswordForm", hasProperty("oldPassword", isEmptyOrNullString())))
                        .andExpect(model().attribute("updatePasswordForm", hasProperty("newPassword", isEmptyOrNullString())))
                        .andExpect(model().attribute("updatePasswordForm", hasProperty("confirmPassword", isEmptyOrNullString())))
                        ;
    
    super.mvc.perform(get("/changePassword?redirect_uri={uri}", "http://example.com"))
                        .andExpect(authenticated())
                        .andExpect(status().isOk())
                        .andExpect(request().sessionAttribute("SPRING_SECURITY_SAVED_REQUEST", hasProperty("queryString", equalTo("redirect_uri=http://example.com"))))
                        .andExpect(model().attribute("updatePasswordForm", hasProperty("oldPassword", isEmptyOrNullString())))
                        .andExpect(model().attribute("updatePasswordForm", hasProperty("newPassword", isEmptyOrNullString())))
                        .andExpect(model().attribute("updatePasswordForm", hasProperty("confirmPassword", isEmptyOrNullString())))
                        .andExpect(model().attribute("updatePasswordForm", hasProperty("redirectUriEnabled", equalTo(true))))
                        ;
    //@formatter:on
  }
  
  @Test
  @WithMockUser(username = TEST2_ID)
  public void test_POST_changePassword_errors() throws Exception {
    //@formatter:off
    super.mvc.perform(post("/changePassword")
                          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                          .with(csrf())
                          .param("action", "save")
                          .param("oldPassword", "invalidOldPassword") //old password is incorrect
                          .param("newPassword", "testtesttest")
                          .param("confirmPassword", "testtesttest"))
                      .andExpect(authenticated()) 
                      .andExpect(status().isOk())
                      .andExpect(view().name("changePassword"))
                      .andExpect(model().attributeHasFieldErrors("updatePasswordForm", "oldPassword"))                      
                      ;
                      
    super.mvc.perform(post("/changePassword")
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .with(csrf())
                            .param("action", "save")
                            .param("oldPassword", "testtest") 
                            .param("newPassword", "testtesttest")
                            .param("confirmPassword", "doesnotmatch")) //confirm password does not match
                        .andExpect(authenticated()) 
                        .andExpect(status().isOk())
                        .andExpect(view().name("changePassword"))
                        .andExpect(model().attributeHasFieldErrors("updatePasswordForm", "confirmPassword"))
                        ;
    //@formatter:on
  }
  
  @Test
  @WithMockUser(username = TEST2_ID)
  public void test_POST_changePassword_success() throws Exception {
    //@formatter:off
    super.mvc.perform(post("/changePassword")
                          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                          .with(csrf())
                          .param("action", "save")
                          .param("oldPassword", "testtest") 
                          .param("newPassword", "testtesttest")
                          .param("confirmPassword", "testtesttest")) //confirm password does not match
                      .andExpect(authenticated()) 
                      .andExpect(status().is2xxSuccessful())
                      .andExpect(view().name("changePassword"))
                      .andExpect(request().attribute("message", hasProperty("text", equalTo("Your password has been changed successfully."))))
                      .andExpect(request().attribute("message", hasProperty("type", equalTo(MessageType.INFO))))
                      ;
    //@formatter:on
  }
  
  
}
