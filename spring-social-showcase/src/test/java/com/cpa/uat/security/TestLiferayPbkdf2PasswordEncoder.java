/**
 * Created on: Jun 27, 2017
 * @author: cpahmn
 */
package com.cpa.uat.security;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.cpa.uaa.security.LiferayPasswordPbkdf2Encoder;

/**
 * @author Hieu Nguyen
 *
 */
public class TestLiferayPbkdf2PasswordEncoder {

  @Test
  public void testPasswordEncoding() {
    final PasswordEncoder pbkdf2PasswordEncoder = new LiferayPasswordPbkdf2Encoder();
    final String rawPassword = "thisismytestpassword";
    String encodedPasss= pbkdf2PasswordEncoder.encode(rawPassword);
    
    assertTrue(pbkdf2PasswordEncoder.matches(rawPassword,encodedPasss));
  }
}
