/**
 * Created on: Jul 28, 2017
 * @author: cpahmn
 */
package com.cpa.uat.config;

import static org.mockito.Mockito.mock;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.cpa.tins.client.MessageWebServiceImplPortType;
import com.cpa.uaa.security.recaptcha.GoogleRecaptchaService;

/**
 * @author Hieu Nguyen
 *
 */
@Configuration
public class TestConfig {

  @Bean
  @Primary
  public MessageWebServiceImplPortType tinsClientService() {
    return mock(MessageWebServiceImplPortType.class);
  }

  @Bean
  @Primary
  public GoogleRecaptchaService googleRecaptchaService() {
    return mock(GoogleRecaptchaService.class);
  }
}
