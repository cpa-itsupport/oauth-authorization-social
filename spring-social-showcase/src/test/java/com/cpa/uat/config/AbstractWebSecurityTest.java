/**
 * Created on: Jul 27, 2017
 * @author: cpahmn
 */
package com.cpa.uat.config;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.cpa.uaa.Application;

/**
 * @author Hieu Nguyen
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = { Application.class,
    TestConfig.class })
@ActiveProfiles("unit-test")
public abstract class AbstractWebSecurityTest {
  
  protected static final String TEST1_ID = "ac7d83f7-13de-438a-ba1d-9cc4c283a669";
  protected static final String TEST2_ID = "12a88495-fcaa-457f-8b7d-cd3ac9d349b7";

  @Autowired
  private FilterChainProxy springSecurityFilterChain;

  protected MockMvc mvc;

  @Autowired
  WebApplicationContext context;

  @Before
  public void beforeEachTest() {
    mvc = MockMvcBuilders.webAppContextSetup(context)
        .apply(SecurityMockMvcConfigurers.springSecurity(springSecurityFilterChain)).build();
  }
}
