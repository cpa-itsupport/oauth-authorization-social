/**
 * Created on: Jul 27, 2017
 * @author: cpahmn
 */
package com.cpa.uat.account;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;

import com.cpa.uat.config.AbstractWebSecurityTest;

/**
 * @author Hieu Nguyen
 *
 */
@Transactional
public class AccountControllerTest extends AbstractWebSecurityTest {

  @Test
  public void test_GET_EditProfile_with_anonymous_user() throws Exception {
    //@formatter:off
    super.mvc.perform(get("/editProfile"))
                      .andExpect(status().is3xxRedirection())
                      .andExpect(redirectedUrl("http://localhost/signin"))
                      .andExpect(unauthenticated())
                      ;
    //@formatter:on
  }

  @Test
  @WithMockUser(username = TEST1_ID)
  public void test_GET_EditProfile_success() throws Exception {
    //@formatter:off
    super.mvc.perform(get("/editProfile"))
                        .andExpect(authenticated())
                        .andExpect(status().isOk())
                        .andExpect(request().sessionAttribute("SPRING_SECURITY_SAVED_REQUEST", nullValue()))
                        .andExpect(model().attribute("editProfileForm", hasProperty("firstName", equalTo("TEST1"))))
                        .andExpect(model().attribute("editProfileForm", hasProperty("lastName", equalTo("TESTER"))))
                        .andExpect(model().attribute("editProfileForm", hasProperty("redirectUriEnabled", equalTo(false))))
                        .andExpect(model().attribute("editProfileForm", hasProperty("email", equalTo("test1@test.com"))));
    
    super.mvc.perform(get("/editProfile?redirect_uri={uri}", "http://blahblah.com"))
                        .andExpect(authenticated())
                        .andExpect(status().isOk())
                        .andExpect(request().sessionAttribute("SPRING_SECURITY_SAVED_REQUEST", hasProperty("queryString", equalTo("redirect_uri=http://blahblah.com"))))
                        .andExpect(model().attribute("editProfileForm", hasProperty("firstName", equalTo("TEST1"))))
                        .andExpect(model().attribute("editProfileForm", hasProperty("lastName", equalTo("TESTER"))))
                        .andExpect(model().attribute("editProfileForm", hasProperty("redirectUriEnabled", equalTo(true))))
                        .andExpect(model().attribute("editProfileForm", hasProperty("email", equalTo("test1@test.com"))));
    //@formatter:on
  }

  @Test
  @WithMockUser(username = TEST2_ID)
  public void test_POST_EditProfile_change_name_success() throws Exception {
  //@formatter:off
    super.mvc.perform(post("/editProfile")
                            .with(csrf())
                            .param("firstName", "TEST22")
                            .param("lastName", "TESTER")
                            .param("email", "test2@test.com")
                            .param("action", "save"))
                        .andExpect(authenticated())
                        .andExpect(status().isOk())
                        .andExpect(model().attribute("editProfileForm", hasProperty("firstName", equalTo("TEST22"))))
                        .andExpect(model().attribute("editProfileForm", hasProperty("lastName", equalTo("TESTER"))))
                        .andExpect(model().attribute("editProfileForm", hasProperty("email", equalTo("test2@test.com"))))
                        .andExpect(request().attribute("message", hasProperty("text", equalTo("Your profile has been updated."))));
    //@formatter:on
  }
  
  @Test
  @WithMockUser(username = TEST2_ID)
  public void test_POST_EditProfile_change_email_success() throws Exception {
  //@formatter:off
    super.mvc.perform(post("/editProfile")
                            .with(csrf())
                            .param("firstName", "TEST22")
                            .param("lastName", "TESTER")
                            .param("email", "test22@test.com")
                            .param("action", "save"))
                        .andExpect(authenticated())
                        .andExpect(status().isOk())
                        .andExpect(model().attribute("editProfileForm", hasProperty("firstName", equalTo("TEST22"))))
                        .andExpect(model().attribute("editProfileForm", hasProperty("lastName", equalTo("TESTER"))))
                        .andExpect(model().attribute("editProfileForm", hasProperty("email", equalTo("test22@test.com"))))
                        .andExpect(request().attribute("message", hasProperty("text", equalTo("Your email has been changed, please logout, check your email to activate it, and login with your new email as the username."))));
    //@formatter:on
  }
  
  @Test
  @WithMockUser(username = TEST2_ID)
  public void test_POST_EditProfile_change_email_failed() throws Exception {
  //@formatter:off
    super.mvc.perform(post("/editProfile")
                            .with(csrf())
                            .param("firstName", "TEST22")
                            .param("lastName", "TESTER")
                            .param("email", "test1@test.com")
                            .param("action", "save"))
                        .andExpect(authenticated())
                        .andExpect(status().isOk())
                        .andExpect(model().attributeHasFieldErrors("editProfileForm","email"))
                        ;
    //@formatter:on
  }

}
