/**
 * Created on: Jul 27, 2017
 * @author: cpahmn
 */
package com.cpa.uat.signin;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;

import com.cpa.uat.config.AbstractWebSecurityTest;

/**
 * @author Hieu Nguyen
 *
 */
public class SigninControllerTest extends AbstractWebSecurityTest {

  @Test
  public void testGetSignIn() throws Exception {
    //@formatter:off
    super.mvc.perform(get("/signin"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(unauthenticated())                
                ;
  //@formatter:on
  }

  @Test
  public void test_POST_signin_with_disabled_account() throws Exception {
  //@formatter:off
    mvc.perform(formLogin("/signin/authenticate").user("test1@test.com").password("testtest"))
                .andExpect(unauthenticated())
                .andExpect(redirectedUrl("/signin?error=disabled_account"))
                ;
  //@formatter:on
  }

  @Test
  public void test_POST_signin_with_wrong_password() throws Exception {
  //@formatter:off
    mvc.perform(formLogin("/signin/authenticate").user("test2@test.com").password("testtest1"))
                .andExpect(unauthenticated())
                .andExpect(redirectedUrl("/signin?error=bad_credentials"))
                ;
  //@formatter:on
  }

  @Test
  public void test_POST_signin_success() throws Exception {
  //@formatter:off
    mvc.perform(formLogin("/signin/authenticate").user("test2@test.com").password("testtest"))
                .andExpect(authenticated())
                .andExpect(redirectedUrl("/connect"))
                ;
  //@formatter:on
  }
}
